/*
	Created by Tech_dog (VToropov) on 6-Dec-2016 11:46:38p, UTC+7, Phuket, Rawai, Tuesday;
	This is Daya Dimensi Global UWP application notification manager class implementation file.
*/
#include "pch.h"
#include "Daya_ToDo_Updater.h"

using namespace Daya_ToDo;
using namespace Windows::Foundation; // time span declaration;

#include "MainPage.xaml.h"

/////////////////////////////////////////////////////////////////////////////

CDataUpdater::CDataUpdater(void) : m_bRunning(false), m_timer(nullptr)
{
}

/////////////////////////////////////////////////////////////////////////////

bool         CDataUpdater::IsRunning(void)const
{
	return m_bRunning;
}

HRESULT      CDataUpdater::Start(void)
{
	m_error = S_OK;

	if (this->IsRunning())
		return m_error;

	static long long sec_ = 10000000;  // 10,000,000 ticks per second

	TimeSpan period_;
#if defined(_DEBUG)
	period_.Duration = 15 *  1 * sec_; // per 15 secs
#else
	period_.Duration = 15 * 60 * sec_; // per 15 mins
#endif
	m_timer = ThreadPoolTimer::CreatePeriodicTimer(
		ref new TimerElapsedHandler([this](ThreadPoolTimer^ source)
		{
			task_ext::run_async_non_interactive(
					ref new DispatchedHandler([this]()
				{
					MainPage::Status()->SetInfoMessage(
						"Auto-update: querying data from web service..."
						);
					MainPage::Instance()->FireDataUpdate();
				}));
		}),
        period_,
		ref new TimerDestroyedHandler([&] (ThreadPoolTimer^ source)
		{
			task_ext::run_async_non_interactive(
				ref new DispatchedHandler([this]()
			{
				MainPage::Status()->SetInfoMessage("Auto-update: the process is stopped");
			}));
		})
	);

	m_bRunning = true;
	return m_error;
}

HRESULT      CDataUpdater::Stop(void)
{
	m_error = S_OK;

	if (!this->IsRunning())
		return m_error;

	if (m_timer)
		m_timer->Cancel();

	m_bRunning = false;
	return m_error;
}