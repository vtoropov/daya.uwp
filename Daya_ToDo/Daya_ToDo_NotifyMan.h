#ifndef _DAYATODONOTIFYMAN_H_3EA67C1C_9011_4400_AED9_3229DF6A1847_INCLUDED
#define _DAYATODONOTIFYMAN_H_3EA67C1C_9011_4400_AED9_3229DF6A1847_INCLUDED
/*
	Created by Tech_dog (VToropov) on 6-Dec-2016 11:19:39p, UTC+7, Phuket, Rawai, Tuesday;
	This is Daya Dimensi Global UWP application notification manager class declaration file.
*/
#include "CommonModel.h"

namespace Daya_ToDo { namespace notify
{
	using Daya_ToDo::CSysError;
	using Daya_ToDo::Data::IGenericProviderSink;

	class CNotifyManager
	{
	private:
		IGenericProviderSink& m_evt_sink;
	private:
		static
		std::wstring m_cache;           // pre-loaded XML template of toast message;
		CSysError    m_error;
	public:
		CNotifyManager(IGenericProviderSink&);
	public:
		TErrorRef    Error(void)const;
		HRESULT      InitializeAsync(void);
		HRESULT      TileNotify (const INT _overdue);
		HRESULT      ToastNotify(const INT _overdue);
	};
}}

#endif/*_DAYATODONOTIFYMAN_H_3EA67C1C_9011_4400_AED9_3229DF6A1847_INCLUDED*/
