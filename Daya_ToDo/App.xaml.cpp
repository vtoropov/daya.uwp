﻿/*
	Created by Tech_dog (VToropov) on 19-Oct-2016 at 11:00:37p, GMT+7, Phuket, Rawai, Wednesday;
	This is Daya Dimensi Global UWP application entry point main class implementation file.
*/

#include "pch.h"
#include "MainPage.xaml.h"

using namespace Daya_ToDo;

using namespace Platform;
using namespace Windows::ApplicationModel;
using namespace Windows::ApplicationModel::Activation;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Interop;

using namespace Windows::UI::Popups;
using namespace Windows::UI::Xaml::Navigation;
/////////////////////////////////////////////////////////////////////////////
/// <summary>
/// Initializes the singleton application object.  This is the first line of authored code
/// executed, and as such is the logical equivalent of main() or WinMain().
/// </summary>
App::App(void)
{
    InitializeComponent();
    Suspending += ref new SuspendingEventHandler(this, &App::OnSuspending);
}

App::~App(void)
{
}

/////////////////////////////////////////////////////////////////////////////

void App::OnActivated(IActivatedEventArgs^ args)
{
	if (args->Kind == ActivationKind::ToastNotification)
	{
#if (0)
		MessageDialog^ popup_ = ref new MessageDialog(_T("Break point. This is the time to attach to me."));
		popup_->ShowAsync();
#endif
		MainPage::SetStartFromAC(true); // TODO: doesn't affect the behaviour of notify manager;

		auto rootFrame = dynamic_cast<Frame^>(Window::Current->Content);
		if ( rootFrame == nullptr)
		{
			rootFrame = ref new Frame();
			rootFrame->NavigationFailed += ref new NavigationFailedEventHandler(this, &App::OnNavigationFailed);
			rootFrame->Navigate(TypeName(MainPage::typeid));
			Window::Current->Content = rootFrame;
			Window::Current->Activate();
			Window::Current->Closed += ref new WindowClosedEventHandler(this, &App::OnMainWindowClose);
		}
	}
}

void App::OnBackgroundActivated(BackgroundActivatedEventArgs^ args)
{
	args;
	BOOL dummy_ = FALSE;
	dummy_ = TRUE;
}

/// <summary>
/// Invoked when the application is launched normally by the end user.  Other entry points
/// will be used such as when the application is launched to open a specific file.
/// </summary>
/// <param name="e">Details about the launch request and process.</param>
void App::OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ e)
{
#if _DEBUG
    // Show graphics profiling information while debugging.
    if (IsDebuggerPresent())
    {
        // Display the current frame rate counters
         DebugSettings->EnableFrameRateCounter = false;
    }
#endif
    auto rootFrame = dynamic_cast<Frame^>(Window::Current->Content);

    // Do not repeat app initialization when the Window already has content,
    // just ensure that the window is active
    if (rootFrame == nullptr)
    {
        // Create a Frame to act as the navigation context and associate it with
        // a SuspensionManager key
        rootFrame = ref new Frame();
        rootFrame->NavigationFailed += ref new Windows::UI::Xaml::Navigation::NavigationFailedEventHandler(this, &App::OnNavigationFailed);

        if (e->PreviousExecutionState == ApplicationExecutionState::Terminated)
        {
            // TODO: Restore the saved session state only when appropriate, scheduling the
            // final launch steps after the restore is complete
        }

        if (e->PrelaunchActivated == false)
        {
            if (rootFrame->Content == nullptr)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                rootFrame->Navigate(TypeName(MainPage::typeid), e->Arguments);
            }
            // Place the frame in the current Window
            Window::Current->Content = rootFrame;
            // Ensure the current window is active
            Window::Current->Activate();
			Window::Current->Closed += ref new WindowClosedEventHandler(this, &App::OnMainWindowClose);
        }
    }
    else
    {
        if (e->PrelaunchActivated == false)
        {
            if (rootFrame->Content == nullptr)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                rootFrame->Navigate(TypeName(MainPage::typeid), e->Arguments);
            }
            // Ensure the current window is active
            Window::Current->Activate();
        }
    }
	Windows::UI::Core::SystemNavigationManager::GetForCurrentView()->
		BackRequested += ref new Windows::Foundation::EventHandler<
		Windows::UI::Core::BackRequestedEventArgs^>(
				this, &App::OnBackRequested
			);
}

/////////////////////////////////////////////////////////////////////////////

void App::OnBackRequested(Platform::Object ^ sender, Windows::UI::Core::BackRequestedEventArgs^ e)
{
	Frame^ rootFrame = dynamic_cast<Frame^>(Window::Current->Content);
	if (rootFrame == nullptr)
		return;

	// Navigate back if possible, and if the event has not
	// already been handled.
	if (rootFrame->CanGoBack && e->Handled == false)
	{
		e->Handled = true;
		rootFrame->GoBack();
	}
}

void App::OnMainWindowClose(Platform::Object^ sender, Windows::UI::Core::CoreWindowEventArgs^ e)
{
	sender; e;
	if (e->Handled)
	{}
}

/// <summary>
/// Invoked when Navigation to a certain page fails
/// </summary>
/// <param name="sender">The Frame which failed navigation</param>
/// <param name="e">Details about the navigation failure</param>
void App::OnNavigationFailed(Platform::Object ^sender, Windows::UI::Xaml::Navigation::NavigationFailedEventArgs ^e)
{
	throw ref new FailureException("Failed to load Page " + e->SourcePageType.Name);
}

/// <summary>
/// Invoked when application execution is being suspended.  Application state is saved
/// without knowing whether the application will be terminated or resumed with the contents
/// of memory still intact.
/// </summary>
/// <param name="sender">The source of the suspend request.</param>
/// <param name="e">Details about the suspend request.</param>
void App::OnSuspending(Object^ sender, SuspendingEventArgs^ e)
{
    (void) sender;  // Unused parameter
    (void) e;   // Unused parameter

    //TODO: Save application state and stop any background activity
}