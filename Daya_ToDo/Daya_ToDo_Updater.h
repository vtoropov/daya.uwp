#ifndef _DAYATODOUPDATER_H_D78BDC2F_9A50_4954_8AF8_1053A394A510_INCLUDED
#define _DAYATODOUPDATER_H_D78BDC2F_9A50_4954_8AF8_1053A394A510_INCLUDED
/*
	Created by Tech_dog (VToropov) on 6-Dec-2016 at 11:19:39p, UTC+7, Phuket, Rawai, Tuesday;
	This is Daya Deminsi Global UWP application data updater (in background mode) class declaration file.
*/
#include "CommonModel.h"

namespace Daya_ToDo
{
	using namespace Windows::System::Threading;
	using namespace Windows::UI::Core;

	class CDataUpdater
	{
	private:
		CSysError        m_error;
		bool             m_bRunning;
		ThreadPoolTimer^ m_timer;
	public:
		CDataUpdater(void);
	public:
		TErrorRef        Error(void)const;
		bool             IsRunning(void)const;
		HRESULT          Start(void);
		HRESULT          Stop(void);
	};
}

#endif/*_DAYATODOUPDATER_H_D78BDC2F_9A50_4954_8AF8_1053A394A510_INCLUDED*/