﻿//
// Created by Tech_dog (VToropov) on 19-Oct-2016 at 10:19:51p, GMT+7, Phuket, Rawai, Wednesday;
// This file is for generating/creating the precompiled headers.
//
#include "pch.h"

namespace task_ext
{
	// allows lower priority (noninteractive) work to be scheduled on the main thread;

	void run_async_non_interactive(std::function<void()>&& action)
	{
		Windows::UI::Core::CoreDispatcher^ dispatcher_ = global::get_core_dispatcher();
		if (dispatcher_ != nullptr)
		{
			dispatcher_->RunAsync(
				Windows::UI::Core::CoreDispatcherPriority::Low,
				ref new Windows::UI::Core::DispatchedHandler([action]()
			{
				action();
			}));
		}
	}
}

namespace global
{
	CoreDispatcher^ get_core_dispatcher(void)
	{
		Windows::UI::Core::CoreWindow^ wnd = Windows::ApplicationModel::Core::CoreApplication::MainView->CoreWindow;
		if (wnd != nullptr)
			return wnd->Dispatcher;
		else
			return nullptr;
	}
}