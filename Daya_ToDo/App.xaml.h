﻿#ifndef _DAYATHEAPPXAML_H_7212D84A_15E8_4619_B45F_B27B5E50C47C_INCLUDED
#define _DAYATHEAPPXAML_H_7212D84A_15E8_4619_B45F_B27B5E50C47C_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Oct-2016 at 10:45:44p, GMT+7, Phuket, Rawai, Wednesday;
	This is Daya Dimensi Global UWP application entry point main class declaration file.
*/
#include "App.g.h"

using namespace Windows::ApplicationModel::Activation;

namespace Daya_ToDo
{
	/// <summary>
	/// Provides application-specific behavior to supplement the default Application class.
	/// </summary>
	ref class App sealed
	{
	protected: // application base class methods' overrides
		virtual void OnActivated(IActivatedEventArgs^ args)   override sealed;
		virtual void OnBackgroundActivated(BackgroundActivatedEventArgs^ args) override sealed;
		virtual void OnLaunched (LaunchActivatedEventArgs^ e) override sealed;

	public:
		App(void);
		virtual ~App(void);

	private:   // auxiliary event handlers
		void OnBackRequested(Platform::Object ^ sender, Windows::UI::Core::BackRequestedEventArgs^ e);
		void OnMainWindowClose(Platform::Object^ sender, Windows::UI::Core::CoreWindowEventArgs^ e);
		void OnNavigationFailed(Platform::Object ^sender, Windows::UI::Xaml::Navigation::NavigationFailedEventArgs ^e);
		void OnSuspending(Platform::Object^ sender, Windows::ApplicationModel::SuspendingEventArgs^ e);
	};
}

#endif/*_DAYATHEAPPXAML_H_7212D84A_15E8_4619_B45F_B27B5E50C47C_INCLUDED*/