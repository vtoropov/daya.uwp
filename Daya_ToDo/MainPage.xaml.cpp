﻿//
// Created by Tech_dog (VToropov) on 19-Oct-2016 at 9:59:04p, GMT+7, Phuket, Rawai, Wednesday;
// This is Daya Dimensi Global UWP application main page class implementation file.
//

#include "pch.h"
#include "MainPage.xaml.h"

using namespace Daya_ToDo;
using namespace Daya_ToDo::Data::Xml;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Media::Animation;
using namespace Windows::Web::Http;
using namespace Windows::UI::Xaml::Controls;

#include <ppltasks.h> 
using namespace Windows::Storage;
using namespace Windows::System;
using namespace concurrency;

#include "HtmlDataProvider.h"
using namespace Daya_ToDo::Data;

#include "UserProfile.h"
#include "UserProfilePage.xaml.h"
#include "Daya_ToDo_Updater.h"

namespace _impl {
	static MainPageStatus^ ctl_status_ = ref new MainPageStatus();
	static MainPage^       main_pg_obj = nullptr;
	static CDataUpdater    updater_;
	static CHtmlFilterEx   filter_;
	static bool            startedFromAC_ = false; // indicates starting the application from Action Center
}

bool          MainPage::m_profileUpdated  = false;
CUserProfile  MainPage::m_profile;

/////////////////////////////////////////////////////////////////////////////

VOID MainPageEventHandler::IGenericProviderSink_OnBegin(const eOperation _oper)
{
	MainPage^ p_main = MainPage::Instance();
	if (p_main == nullptr)
		return;

	switch (_oper)
	{
	case eOperation::eOperHttpQueryStarted:
		{
		if (_impl::updater_.IsRunning())
			_impl::updater_.Stop(); // stops auto-update till completing HTML content generating;
			p_main->Status()->SetWaitMessage("Waiting for web service response...");
			p_main->ShowProgress(true);
		}break;
	case eOperation::eOperXslCacheInitStarted:
	case eOperation::eOperXslLinkInitStarted:
		{
			p_main->Status()->SetWaitMessage("Initializing XSL stylesheet data...");
		} break;
	case eOperation::eOperHtmlOpenStarted:
		{
			p_main->Status()->SetWaitMessage("Processing data...");
		} break;
	}
}

VOID MainPageEventHandler::IGenericProviderSink_OnError(const CSysError _err)
{
	if (_impl::main_pg_obj == nullptr)
		return;

	Platform::String^ details_ = ref new Platform::String(_err.GetFormattedDetails().c_str());
	_impl::main_pg_obj->Status()->SetWarnMessage(details_);
	_impl::main_pg_obj->ShowProgress(false);
}

VOID MainPageEventHandler::IGenericProviderSink_OnFinish(const eOperation _oper)
{
	MainPage^ p_main = MainPage::Instance();
	if (p_main == nullptr)
		return;

	switch (_oper)
	{
	case eOperation::eOperHtmlInitFinished:
		{
			task_ext::run_async_non_interactive([p_main]()
			{
				p_main->Frame->Navigate(Daya_ToDo::MainPage::typeid);
				return;
			});
		} break;
	case eOperation::eOperHttpQueryFinished:
		{
			p_main->Status()->SetInfoMessage("Data has been received from web service");
			p_main->m_xml_provider.XslCache().InitializeXslCacheAsync();
		} break;
	case eOperation::eOperXslCacheInitFinished:
		{
			p_main->m_xml_provider.Resolver().InitializeXslCacheAsync();
		} break;
	case eOperation::eOperXslLinkInitFinished:
		{
			p_main->m_htm_provider.CreateContentAsync(
				_impl::filter_ ,
				MainPage::m_profile,
				p_main->m_web_provider.Cache()
			);
			CXmlDataExtension ext_;
			ext_.ExtractData(
				p_main->m_web_provider.Cache()
			);

		} break;
	case eOperation::eOperXmlParsingFinished:
		{
		} break;
	case eOperation::eOperHtmlOpenFinished:
		{
			p_main->WebViewObject->NavigateToString(
				p_main->m_htm_provider.Content()
			);
			p_main->Status()->SetInfoMessage("Ready");
			p_main->ShowProgress(false);
			p_main->m_notify_man.InitializeAsync();

			if (_impl::updater_.IsRunning() == false)
				_impl::updater_.Start();  // (re-)starts auto-updater;
		} break;
	case eOperation::eOperNotifyInitFinished:
		{
			CXmlDataExtension ext_;
			if (ext_.OverdueDaysChanged()
				&& !_impl::startedFromAC_)
					p_main->m_notify_man.ToastNotify(ext_.OverdueDays());

			p_main->m_notify_man.TileNotify(ext_.OverdueDays());

			if (_impl::startedFromAC_)
			{
				_impl::startedFromAC_ = false;  // (re-)sets the flag of starting from action center;
				ext_.OverdueDaysChanged(false);
			}
		} break;
	}
}

/////////////////////////////////////////////////////////////////////////////

MainPage::MainPage() :
	m_web_provider(m_evt_handler), m_htm_provider(m_evt_handler), m_xml_provider(m_evt_handler), m_notify_man(m_evt_handler)
{
	_impl::main_pg_obj = this;
	InitializeComponent();

	this->WebViewObject->ScriptNotify += ref new NotifyEventHandler(this, &MainPage::Web_OnScriptNotify);

	m_htm_provider.InitializeAsync();
}

/////////////////////////////////////////////////////////////////////////////

MainPage^         MainPage::Instance(void)
{
	return _impl::main_pg_obj;
}

MainPageStatus^   MainPage::Status (void)
{
	return _impl::ctl_status_;
}

/////////////////////////////////////////////////////////////////////////////

VOID MainPage::OnNavigatedFrom(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e)
{
	e;
	if (_impl::updater_.IsRunning())
		_impl::updater_.Stop();
}

VOID MainPage::OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e)
{
	if (this->m_htm_provider.IsContentAvailable() && 
		!MainPage::m_profileUpdated)
	{
		//
		// it looks like a user press [Cancel] button in profile page,
		// but data is available from previous update;
		//
		Platform::String^ content_ = this->m_htm_provider.Content();
		this->WebViewObject->NavigateToString(content_);
		if (_impl::updater_.IsRunning() == false)
			_impl::updater_.Start();  // (re-)starts auto-updater;
		return;
	}
	else
	{
		//
		// displays default page without data and prepares to retrieving data from web
		//
		Platform::String^ default_ = ref new Platform::String(
									this->m_htm_provider.DefaultPage().c_str()
				);
		if (nullptr == default_)
			return;
		this->WebViewObject->NavigateToString(default_);
	}
	HRESULT hr_ = S_OK;

	if (MainPage::m_profile.IsValid() == false)
	{
		CUserProfilePersistent pers_;
		hr_ = pers_.Load();
		if (SUCCEEDED(hr_) && pers_.IsValid())
		{
			MainPage::m_profile = pers_;
			MainPage::m_profileUpdated = true;
		}
		else
		{
			task_ext::run_async_non_interactive([this]()
			{
				this->Frame->Navigate(Daya_ToDo::UserProfilePage::typeid);
				return;
			});
		}
	}
	//
	// if profile is valid: either is updated by a user or is restored from app settings,
	// sends a query to web service for data retrieving;
	//
	if (SUCCEEDED(hr_) && MainPage::m_profileUpdated)
	{
		MainPage::m_profileUpdated = false;
		m_web_provider.RetrieveData(MainPage::m_profile);
	}
}

VOID MainPage::OnSizeChanged(Platform::Object^ _sender, Windows::UI::Core::WindowSizeChangedEventArgs^ _args)
{
	_args->Handled = false;
}

/////////////////////////////////////////////////////////////////////////////

VOID MainPage::Web_NavigationCompleted(Windows::UI::Xaml::Controls::WebView^ _sender, Windows::UI::Xaml::Controls::WebViewNavigationCompletedEventArgs^ _args)
{
	_sender; _args;
	if (_args->Uri != nullptr)
	{
		Platform::String^ url_ = _args->Uri->DisplayUri;
		if (url_)
		{}
	}
//	this->m_htm_provider.Style().ApplyTo(this->WebViewObject);
}

VOID MainPage::Web_NavigationStarted(Windows::UI::Xaml::Controls::WebView^ _sender, Windows::UI::Xaml::Controls::WebViewNavigationStartingEventArgs^ _args)
{
	_sender; _args;
	Uri^ uri = _args->Uri;
	if (uri != nullptr)
	{
		String^ url_ = uri->DisplayUri;
		String^ cmd_ = m_htm_provider.CommandFromUrl(url_);
		if (cmd_ != nullptr)
		{
			_args->Cancel = true;
			if ("cmd_tsk_show_settings" == cmd_)
			{
				this->Frame->Navigate(Daya_ToDo::UserProfilePage::typeid);
			}
			else if (_impl::filter_.UpdateFromCommand(cmd_))
			{
				this->Status()->SetWaitMessage("Processing data...");
				this->ShowProgress(true);
				{
					this->m_htm_provider.CreateContentAsync(
						_impl::filter_,
						MainPage::m_profile,
						this->m_web_provider.Cache()
					);
				}
			}
		}
		else // looks like external reference/deep link
		{
			_args->Cancel = true;
			this->Status()->SetWaitMessage("Opening deep link in default browser...");
			this->ShowProgress(true);

			concurrency::task<bool> launchUriOperation(Windows::System::Launcher::LaunchUriAsync(uri));
			launchUriOperation.then([this](bool _success)
			{
				this->Status()->SetInfoMessage("Ready");
				this->ShowProgress(false);
			});
		}
	}
}

VOID MainPage::Web_OnScriptNotify(Platform::Object^ _sender, Windows::UI::Xaml::Controls::NotifyEventArgs^ _args)
{
	Platform::String^ cmd_ = _args->Value;
	Uri^ uri_ = ref new Uri(cmd_);
	this->WebViewObject->Navigate(uri_);
}

/////////////////////////////////////////////////////////////////////////////

VOID MainPage::FireDataUpdate(void)
{
	if (this->m_web_provider.IsBusy())
		return;
	m_web_provider.RetrieveData(MainPage::m_profile);
}

VOID MainPage::SetStartFromAC(bool _value)
{
}

VOID MainPage::ShowProgress(bool _show)
{
	if (_show)
	{
		ProgressRing->Visibility = Windows::UI::Xaml::Visibility::Visible;
		ProgressRing->IsActive = true;
	}
	else
	{
		ProgressRing->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
		ProgressRing->IsActive = false;
	}
}