﻿#ifndef _DAYAUSERPROFILEPAGE_H_0D1596DC_57CA_46F7_8B5E_C43BCC3D3436_INCLUDED
#define _DAYAUSERPROFILEPAGE_H_0D1596DC_57CA_46F7_8B5E_C43BCC3D3436_INCLUDED
/*
	Created by Tech_dog (VToropov) on 25-Oct-2016 at 11:17:36p, UTC+7, Phuket, Rawai, Tuesday;
	This is Daya Dimensi Global UWP application user profile UI page class declaration file.
*/
#include "UI\pages\UserProfilePage.g.h"
#include "UserProfile.h"

namespace Daya_ToDo
{
	using namespace Daya_ToDo::Data;

	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class UserProfilePage sealed
	{
	private:
		CUserProfilePersistent  m_pers;
	public:
		UserProfilePage(VOID);
	private:
		VOID Button_Apply_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		VOID Button_Cancel_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		VOID Text_Comp_OnKeyUp(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		VOID Text_Pass_OnKeyUp(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		VOID Text_User_OnKeyUp(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
	};
}
#endif/*_DAYAUSERPROFILEPAGE_H_0D1596DC_57CA_46F7_8B5E_C43BCC3D3436_INCLUDED*/