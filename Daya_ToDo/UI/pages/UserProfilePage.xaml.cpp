﻿/*
	Created by Tech_dog (VToropov) on 25-Oct-2016 at 11:17:36p, UTC+7, Phuket, Rawai, Tuesday;
	This is Daya Dimensi Global UWP application user profile UI page class implementation file.
*/

#include "pch.h"
#include "UserProfilePage.xaml.h"

using namespace Daya_ToDo;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Media::Animation;

#include "MainPage.xaml.h"

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

/////////////////////////////////////////////////////////////////////////////

UserProfilePage::UserProfilePage(VOID)
{
	InitializeComponent();
	HRESULT hr_ = m_pers.Load();
	if (SUCCEEDED(hr_))
	{
		this->txtCompanyId->Text = ref new Platform::String(m_pers.CoIdentifier().c_str());
		this->txtPassword->Password = ref new Platform::String(m_pers.Password().c_str());
		this->txtUserName->Text = ref new Platform::String(m_pers.UserName().c_str());
	}
	this->btnApply->IsEnabled = m_pers.IsValid();
}

/////////////////////////////////////////////////////////////////////////////

VOID UserProfilePage::Button_Apply_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	m_pers.Save();
	if (MainPage::Instance()->Frame->CanGoBack)
	{
		MainPage::m_profileUpdated = true;
		MainPage::m_profile = m_pers;
		MainPage::Instance()->Frame->GoBack();
	}
}

VOID UserProfilePage::Button_Cancel_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	if (MainPage::Instance()->Frame->CanGoBack)
	{
		MainPage::m_profileUpdated = false;
		MainPage::Instance()->Frame->GoBack();
	}
}

VOID UserProfilePage::Text_Comp_OnKeyUp(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	m_pers.CoIdentifier(this->txtCompanyId->Text->Data());
	this->btnApply->IsEnabled = m_pers.IsValid();
}

VOID UserProfilePage::Text_Pass_OnKeyUp(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	m_pers.Password(this->txtPassword->Password->Data());
	this->btnApply->IsEnabled = m_pers.IsValid();
}

VOID UserProfilePage::Text_User_OnKeyUp(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	m_pers.UserName(this->txtUserName->Text->Data());
	this->btnApply->IsEnabled = m_pers.IsValid();
}