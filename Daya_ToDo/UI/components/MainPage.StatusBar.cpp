/*
	Created by Tech_dog (VToropov) 9-Dec-2016 at 9:49:20p, UTC+7, Phuket, Rawai, Friday;
	This is Days Dimensi Global UWP application main window status bar class implementation file.
*/
#include "pch.h"
#include "MainPage.StatusBar.h"
#include "MainPage.xaml.h"

using namespace Daya_ToDo;

using namespace Windows::Foundation;               // for URI;
using namespace Windows::UI::Xaml::Media::Imaging; // for BitmapImage

/////////////////////////////////////////////////////////////////////////////

MainPageStatus::MainPageStatus(void){}

/////////////////////////////////////////////////////////////////////////////

VOID MainPageStatus::SetInfoMessage(Platform::String^ strMessage)
{
	this->SetStatus(strMessage, eNotifyType::eInfoMessage);
}

VOID MainPageStatus::SetWaitMessage(Platform::String^ strMessage)
{
	this->SetStatus(strMessage, eNotifyType::eWaitMessage);
}

VOID MainPageStatus::SetWarnMessage(Platform::String^ strMessage)
{
	this->SetStatus(strMessage, eNotifyType::eErrorMessage);
}

VOID MainPageStatus::SetStatus(Platform::String^ strMessage, eNotifyType eType)
{
	MainPage^ p_main = MainPage::Instance();
	if (p_main == nullptr)
		return;

	strMessage; eType;
	switch (eType)
	{
	case eNotifyType::eErrorMessage: p_main->StatusImage->Source = ref new BitmapImage(ref new Uri("ms-appx:///Assets/images/ddg_stat_img_warn_16x16px.png")); break;
	case eNotifyType::eInfoMessage : p_main->StatusImage->Source = ref new BitmapImage(ref new Uri("ms-appx:///Assets/images/ddg_stat_img_info_16x16px.png")); break;
	case eNotifyType::eWaitMessage : p_main->StatusImage->Source = ref new BitmapImage(ref new Uri("ms-appx:///Assets/images/ddg_stat_img_wait_16x16px.png")); break;
	default:;
	}
	if (eNotifyType::eErrorMessage == eType) p_main->StatusMessage->Text = "Error: " + strMessage;
	else                                     p_main->StatusMessage->Text = strMessage;
}
