#ifndef _MAINPAGESTATUSBAR_H_D500EF2C_BD53_4B1C_80A7_801093081893_INCLUDED
#define _MAINPAGESTATUSBAR_H_D500EF2C_BD53_4B1C_80A7_801093081893_INCLUDED
/*
	Created by Tech_dog (VToropov) 9-Dec-2016 at 9:38:38p, UTC+7, Phuket, Rawai, Friday;
	This is Days Dimensi Global UWP application main window status bar class declaration file.
*/
namespace Daya_ToDo
{
	public enum class eNotifyType
	{
		eInfoMessage  ,
		eErrorMessage ,
		eWaitMessage  ,
	};

	public ref class MainPageStatus sealed
	{
	public:
		MainPageStatus(void);
	public:
		VOID SetInfoMessage(Platform::String^ strMessage);
		VOID SetWaitMessage(Platform::String^ strMessage);
		VOID SetWarnMessage(Platform::String^ strMessage);
		VOID SetStatus(Platform::String^ strMessage, eNotifyType eType);
	};
}

#endif/*_MAINPAGESTATUSBAR_H_D500EF2C_BD53_4B1C_80A7_801093081893_INCLUDED*/