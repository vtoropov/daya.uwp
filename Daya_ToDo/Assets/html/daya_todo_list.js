//
// Created by Tech_dog (VToropov) on 7-Jul-2016 at 3:20:36p, GMT+7, Phuket, Rawai, Thursday;
// This is Daya Dimensi Global desktop java script function(s) for internal use of the HTML;
//
//

var clrTaskHeaderHover  = "#ffe4b7";
var clrTaskHeaderNormal = "#f5f5f5";

function fn_on_sec_click(parent_id)
{
    var img  = document.getElementById(parent_id + "_img");
    var cnt  = document.getElementById(parent_id + "_cnt");

    if (null != img)
    {
        if (img.className == "section_exp")
            img.className =  "section_col";
        else
            img.className =  "section_exp";
    }
    if (null != cnt)
    {
        if (cnt.style.display == "none")
            cnt.style.display = "block";
        else
            cnt.style.display = "none";
    }
    else
        alert("object " + parent_id + "_cnt is not found");
}

function fn_on_sub_click(parent_id)
{
    var sym  = document.getElementById(parent_id + "_sub_sym");
    var cnt  = document.getElementById(parent_id + "_sub_cnt");
    
    if (null != sym)
    {
        if (sym.innerText == "6")
            sym.innerHTML  = "&#53;";
        else
            sym.innerHTML  = "&#54;";
    }
    if (null != cnt)
    {
        if (cnt.style.display == "none")
            cnt.style.display = "block";
        else
            cnt.style.display = "none";
    }
    else
        alert("object " + parent_id + "_sub_cnt is not found");
}

function fn_on_sub_over(object_id)
{
    var obj_ = document.getElementById(object_id);
    if (obj_ == null)
        return;

    obj_.style.backgroundColor = clrTaskHeaderHover;

    var cnt_ = document.getElementById(object_id + "_sub_cnt");
    if (cnt_ == null)
        return;
    var exp_ = (cnt_.style.display == "block");
    if (exp_)
        return;
    var sym_ = document.getElementById(object_id + "_sub_sym");
    if (sym_ == null)
        return;
    sym_.innerHTML  = "&#54;";
}

function fn_on_sub_leave(object_id)
{
    var obj_ = document.getElementById(object_id);
    if (obj_ == null)
        return;

    obj_.style.backgroundColor = clrTaskHeaderNormal;

    var cnt_ = document.getElementById(object_id + "_sub_cnt");
    if (cnt_ == null)
        return;
    var exp_ = (cnt_.style.display == "block");
    if (exp_)
        return;
    var sym_ = document.getElementById(object_id + "_sub_sym");
    if (sym_ == null)
        return;
    sym_.innerHTML  = "";
}

/////////////////////////////////////////////////////////////////////////////

function fn_down_sel_status()
{
    var ctrl = document.getElementById("ctrl_sel_status_id");
    if (ctrl == null)
        return;

    var cmd_ = ctrl.options[ctrl.selectedIndex].value;
    window.external.notify(cmd_);
}