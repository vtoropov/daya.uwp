<?xml version="1.0" encoding="Windows-1252"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:m="urn:1"
    xmlns:d="urn:2"
    version="1.0">

    <xsl:template name="to-do-task-url">
        <xsl:param name="cat-id"/>
        <xsl:choose>

            <xsl:when test="($cat-id = 9 or $cat-id = 19)"> <!-- live profile -->
                <xsl:value-of select="d:url"/>&#38;password=<xsl:value-of select="$global-user-pass"/>&#38;username=<xsl:value-of select="$global-user-name"/>
            </xsl:when>

            <xsl:when test="($cat-id = 15 or $cat-id = 16 or $cat-id = 26)"> <!-- learning -->
                <xsl:value-of select="d:url"/>&#38;password=<xsl:value-of select="$global-user-pass"/>&#38;username=<xsl:value-of select="$global-user-name"/>&#38;company=<xsl:value-of select="$global-company-id"/>
            </xsl:when>

            <xsl:when test="($cat-id =  5 or $cat-id = 21)"> <!-- recruiting -->
                <xsl:value-of select="d:url"/>&#38;password=<xsl:value-of select="$global-user-pass"/>&#38;username=<xsl:value-of select="$global-user-name"/>&#38;company=<xsl:value-of select="$global-company-id"/>
            </xsl:when>

            <xsl:when test="($cat-id = 20 or $cat-id = 27)"> <!-- on boarding -->
                <xsl:value-of select="d:url"/>&#38;password=<xsl:value-of select="$global-user-pass"/>&#38;username=<xsl:value-of select="$global-user-name"/>&#38;company=<xsl:value-of select="$global-company-id"/>
            </xsl:when>
            
            <xsl:when test="($cat-id = 14 or $cat-id = 17 or $cat-id = 18 or $cat-id = 24 or $cat-id = 25)"> <!-- work flow -->
                <xsl:value-of select="d:url"/>&#38;password=<xsl:value-of select="$global-user-pass"/>            
            </xsl:when>

            <xsl:otherwise>
https://performancemanager10.successfactors.com/sf/pmreviews?
company=<xsl:value-of select="$global-company-id"/>&#38;
username=<xsl:value-of select="$global-user-name"/>&#38;
password=<xsl:value-of select="$global-user-pass"/>&#38;
fbacme_o=my_forms&#38;myf%5fos=inbox&#38;myf%5fss=&#38;myf%5fmode=&#38;
inbox%5ffbfl%5fftype=Review&#38;inbox%5fos=list&#38;reqOrig=pm&#38;page%5fno=1&#38;
inbox%5ffbfl%5fflist%5fact=open%5fform&#38;inbox%5ffbfl%5fftype=Review&#38;
inbox%5ffbfl%5fflist%5ffmid=<xsl:value-of select="d:formDataId"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="to-do-view-url">
https://performancemanager10.successfactors.com/sf/pmreviews?
company=<xsl:value-of select="$global-company-id"/>&#38;
username=<xsl:value-of select="$global-user-name"/>&#38;
password=<xsl:value-of select="$global-user-pass"/>
    </xsl:template>


</xsl:stylesheet>