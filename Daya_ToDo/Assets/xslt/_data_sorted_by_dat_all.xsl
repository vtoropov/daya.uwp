<?xml version="1.0" encoding="Windows-1252"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:m="urn:1"
    xmlns:d="urn:2"
    version="1.0">

    <xsl:param name ="global-company-id"><xsl:value-of select="'${global_cid_value}'"/></xsl:param>    
    <xsl:param name ="global-user-name" ><xsl:value-of select="'${global_usr_value}'"/></xsl:param>
    <xsl:param name ="global-user-pass" ><xsl:value-of select="'${global_pwd_value}'"/></xsl:param>

    <xsl:import href="_data_common_img_defs.xsl"/>
    <xsl:import href="_data_common_url_defs.xsl"/>
    <xsl:import href="_data_common_task_defs.xsl"/>
    <xsl:import href="_data_common_task_one_step.xsl"/>
    <xsl:import href="_data_common_task_multi_step.xsl"/>

    <xsl:template match="/">
        <xsl:call-template  name="date-group">
            <xsl:with-param name="group-name" select='"Due Now"'/>
            <xsl:with-param name="group-id"   select="1"/>
        </xsl:call-template>
        <xsl:call-template  name="date-group">
            <xsl:with-param name="group-name" select='"Due Anytime"'/>
            <xsl:with-param name="group-id"   select="2"/>
        </xsl:call-template>
        <xsl:call-template  name="date-group">
            <xsl:with-param name="group-name" select='"Recently Completed"'/>
            <xsl:with-param name="group-id"   select="3"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="date-group">
        <xsl:param name="group-name"/>
        <xsl:param name="group-id"  />

            <div class="section" id="{$group-id}" onclick="fn_on_sec_click(this.id)">
               <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <colgroup>
                        <col width="10px"/>
                        <col width="24px"/>
                        <col width="100%"/>
                    </colgroup>
                    <tr>
                        <td><div style="width=10px"></div></td>
                        <td><div class="section_col" id="{$group-id}_img">
                                           &#160;&#160;
                        </div></td>
                        <td class="section_title">
                            <xsl:value-of select="$group-name"/> (
                               <xsl:call-template  name="group-task-count">
                                   <xsl:with-param name="group-id"   select="$group-id"/>
                               </xsl:call-template>
                            )
                        </td>
                    </tr>
               </table>
            </div>
            <div id="{$group-id}_cnt" style="display:none">
                <xsl:apply-templates select="feed/entry/content/m:properties/d:todos">
                    <xsl:with-param name="group-id" select="$group-id"/>
                </xsl:apply-templates>
            </div>

    </xsl:template>
    
    <xsl:template match="feed/entry/content/m:properties/d:todos">
        <xsl:param name="group-id"/>
        <xsl:for-each select="d:element">
            <xsl:choose>
                <xsl:when test="($group-id = 1) and (d:dueDateOffSet    = 0)">
                    <xsl:call-template name="task-type-selector"/>
                </xsl:when>

                <xsl:when test="($group-id = 1) and (d:dueDateOffSet &gt; 0)">
                    <xsl:call-template name="task-type-selector"/>
                </xsl:when>

                <xsl:when test="($group-id = 2) and (d:dueDateOffSet &lt; 0)">
                    <xsl:call-template name="task-type-selector"/>
                </xsl:when>
                <xsl:when test="($group-id = 3) and (d:status=3)">
                    <xsl:call-template name="task-type-selector"/>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="task-type-selector">
           <xsl:choose>
                <xsl:when test="count(d:entries/d:element) > 0">
                    <xsl:call-template name="multi-step-task">
                       <xsl:with-param name="objId"      select="d:entryId"/>
                       <xsl:with-param name="catId"      select="d:categoryId"/>
                       <xsl:with-param name="title"      select="d:stepDescAlt"/>
                       <xsl:with-param name="overdue"    select="d:dueDateOffSet"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="single-step-task">
                       <xsl:with-param name="objId"     select="d:entryId"/>
                       <xsl:with-param name="catId"     select="d:categoryId"/>
                       <xsl:with-param name="overdue"   select="d:dueDateOffSet"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
    </xsl:template>

    <xsl:template name="group-task-count">
        <xsl:param name="group-id"/>
        <xsl:choose>
            <xsl:when test="$group-id = 1">
                <xsl:value-of select="count(feed/entry/content/m:properties/d:todos/d:element[d:dueDateOffSet &gt;= 0])"/>
            </xsl:when>
            <xsl:when test="$group-id = 2">
                <xsl:value-of select="count(feed/entry/content/m:properties/d:todos/d:element[d:dueDateOffSet &lt; 0])"/>
            </xsl:when>
            <xsl:when test="$group-id = 3">
                <xsl:value-of select="count(feed/entry/content/m:properties/d:todos/d:element[d:status=3])"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="0"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>