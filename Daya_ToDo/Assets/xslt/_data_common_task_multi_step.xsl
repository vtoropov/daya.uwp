<?xml version="1.0" encoding="Windows-1252"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:m="urn:1"
    xmlns:d="urn:2"
    version="1.0">

    <xsl:template  name="multi-step-task">
        <xsl:param name="objId"  />
        <xsl:param name="catId"  />
        <xsl:param name="title"  />
        <xsl:param name="overdue"/>
        <xsl:variable name="steps"      select="count(d:entries/d:element)"/>
        <xsl:variable name="completed"  select="count(d:entries/d:element[d:status = 3])"/>
        <xsl:variable name="tsk_upcome" select="count(d:entries/d:element[d:status = 1])"/>
        <xsl:variable name="tsk_active" select="count(d:entries/d:element[d:status = 2])"/>

        <xsl:variable name="img_src">
            <xsl:call-template name="cat-id-to-image-name">
                <xsl:with-param name="catId" select="$catId"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="over_cls">
            <xsl:call-template name="overdue-days-to-class">
                <xsl:with-param name="over_days" select="$overdue"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="over_txt">
            <xsl:call-template name="overdue-days-to-text">
                <xsl:with-param name="over_days" select="$overdue"/>
            </xsl:call-template>
        </xsl:variable>
        <div id="{$objId}" 
             onclick="fn_on_sub_click(this.id)"
             style="cursor:hand;height:50px;overflow-y:hidden;"
             onmouseover="fn_on_sub_over('{$objId}')"
             onmouseout="fn_on_sub_leave('{$objId}')">
            <table style="border:1;cellpadding:5px;cellspacing:0;width:97%;height:100%;" class="tsk_list">
                <colgroup>
                    <col width="16px"/>
                    <col width="64px" align="center"/>
                    <col width="32px"/>
                    <col width="100%"/>
                    <col width="32px"/>
                </colgroup>
                <tr valign="middle" height="50px">
                    <td><div></div></td>
                    <td class="{$over_cls}"><div style="width:70px;"><xsl:value-of select="$over_txt"/></div></td>
                    <td><img src="{$img_src}"/></td>
                    <td>
                        <span class="link_color">
                        <xsl:value-of select="$title"/>
                        </span>
                        <br/>
        <xsl:choose>
            <xsl:when test="($tsk_active > 0)">
                        <span class="subtitle">
                            Completed <xsl:value-of select="$completed"/> of <xsl:value-of select="$steps"/>
                        </span>
            </xsl:when>
            <xsl:otherwise>
                        <span class="subtitle">
                            Upcoming <xsl:value-of select="$tsk_upcome"/>
                        </span>
            </xsl:otherwise>
        </xsl:choose>
                    </td>
                    <td id="{$objId}_sub_sym" class="subtask_exp"></td>
                </tr>
            </table>
        </div>

        <div id="{$objId}_sub_cnt" style="display:none">
            <table style="border:0;cellpadding:px;cellspacing:0;width:100%;height:100%" class="tsk_list">
                <colgroup>
                    <col width="80px"/>
                    <col width="50%" />
                    <col width="50%" />
                </colgroup>
                <xsl:apply-templates select="d:entries"/>
            </table>
        </div>

    </xsl:template>
    
</xsl:stylesheet>