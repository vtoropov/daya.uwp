<?xml version="1.0" encoding="Windows-1252"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:m="urn:1"
    xmlns:d="urn:2"
    version="1.0">
    
    <xsl:template match="d:entries">
        <xsl:variable name="tsk_upcome" select="count(d:element[d:status = 1])"/>
        <xsl:variable name="tsk_active" select="count(d:element[d:status = 2])"/>

        <xsl:variable name="tsk_cat" select="parent::*/d:categoryId"/>

        <table border="0" cellpadding="5px" cellspacing="0" width="100%" height="0" class="tsk_list" bgcolor="#efefef">
            <colgroup>
                <col width="22%"/>
                <col width="39%"/>
                <col width="39%"/>
            </colgroup>

        <xsl:choose>
            <xsl:when test="($tsk_upcome > 0 and $tsk_active > 0)">
                <tr style="height:40px;">
                    <td><div style="width:90px"></div></td><td><b>&#160;To Do</b></td><td><b>&#160;Upcoming</b></td>
                </tr>
                <tr valign="top">
                    <td><div style="width:90px"></div></td>
                    <td>
                        <xsl:call-template  name="task-list">
                            <xsl:with-param name="task-type" select="2"/>
                            <xsl:with-param name="task-cat"  select="$tsk_cat"/>
                        </xsl:call-template>
                    </td>
                    <td>
                        <xsl:call-template  name="task-list">
                            <xsl:with-param name="task-type" select="1"/>
                            <xsl:with-param name="task-cat"  select="$tsk_cat"/>
                        </xsl:call-template>
                    </td>
                </tr>
                <tr style="height:40px;valign:bottom;">
                    <td><div style="width:90px"></div></td>
                    <td>
                        &#160;
                        <a>
                            <xsl:attribute name="href">
                                <xsl:call-template name="to-do-view-url"/>
                            </xsl:attribute>
                            View Details
                        </a>
                    </td>
                    <td></td>
                </tr>
            </xsl:when>
            <xsl:when test="($tsk_upcome = 0 and $tsk_active > 0)">
                <tr style="height:40px;">
                    <td><div style="width:90px"></div></td><td><b>&#160;To Do</b></td><td><b></b></td>
                </tr>
                <tr valign="top">
                    <td><div style="width:90px"></div></td>
                    <td>
                        <xsl:call-template  name="task-list">
                            <xsl:with-param name="task-type" select="2"/>
                            <xsl:with-param name="task-cat"  select="$tsk_cat"/>
                        </xsl:call-template>
                    </td>
                </tr>
                <tr style="height:40px;valign:bottom;">
                    <td><div style="width:90px"></div></td>
                    <td>
                        &#160;
                        <a>
                            <xsl:attribute name="href">
                                <xsl:call-template name="to-do-view-url"/>
                            </xsl:attribute>
                            View Details
                        </a>
                    </td>
                    <td></td>
                </tr>
            </xsl:when>
            <xsl:when test="($tsk_upcome > 0 and $tsk_active = 0)">
                <tr style="height:40px;">
                    <td><div style="width:90px"></div></td><td><b>&#160;Upcoming</b></td><td><b></b></td>
                </tr>
                <tr valign="top">
                    <td><div style="width:90px"></div></td>
                    <td>
                        <xsl:call-template  name="task-list">
                            <xsl:with-param name="task-type" select="1"/>
                            <xsl:with-param name="task-cat"  select="$tsk_cat"/>
                        </xsl:call-template>
                    </td>
                </tr>
            </xsl:when>
            <xsl:otherwise>
            </xsl:otherwise>
        </xsl:choose>

        </table>

    </xsl:template>

    <xsl:template  name="task-list">
        <xsl:param name="task-type"/>
        <xsl:param name="task-cat"/>

        <table border="0" cellpadding="0" cellspacing="5" width="100%" class="tsk_list">

        <xsl:for-each select="d:element">
            <xsl:choose>
                <xsl:when test="(d:status = $task-type and d:status = 2)">
                    <tr>
                        <td>
                            <a>
                                <xsl:attribute name="href">
                                    <xsl:call-template name="to-do-task-url">
                                        <xsl:with-param name="cat-id" select="$task-cat"/>
                                    </xsl:call-template>
                                </xsl:attribute>
                                <xsl:value-of select="d:subjectFullName"/>
                            </a>
<!--
                            <br/>
                            <xsl:value-of select="$task-cat"/>
-->
                        </td>
                    </tr>
                </xsl:when>
                <xsl:when test="(d:status = $task-type and d:status = 1)">
                    <tr><td><xsl:value-of select="d:subjectFullName"/></td></tr>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
                                                                                               
        </table>

    </xsl:template>

    
    <xsl:template name="overdue-days-to-class">
        <xsl:param name="over_days"/>
        <xsl:choose>
            <xsl:when test="$over_days > 0">
                <xsl:text>overdue_task</xsl:text>
            </xsl:when>
             <xsl:otherwise>
                <xsl:text>intime_task</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="overdue-days-to-text">
        <xsl:param name="over_days"/>
        <xsl:choose>
            <xsl:when test="$over_days > 0">
                OVERDUE <xsl:value-of select="$over_days"/> DAYS
            </xsl:when>
            <xsl:when test="$over_days = 0">
                TODAY
            </xsl:when>
             <xsl:otherwise>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>