<?xml version="1.0" encoding="Windows-1252"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">

    <xsl:template name="cat-id-to-image-name">
        <xsl:param name="catId"/>
        <xsl:choose>
            <xsl:when test="$catId = 2"><!--CTodoCatId::e360Def-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_perform.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId = 8"><!--CTodoCatId::eBizProcPlatform-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_perform.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId = 0"><!--CTodoCatId::ePerfReviewDef-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_perform.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId =18"><!--CTodoCatId::eAbsenceManagement-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_people.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId =10"><!--CTodoCatId::eInterviewAssess-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_people.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId =14"><!--CTodoCatId::eHrisEmpChangeReq26-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_people.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId =21"><!--CTodoCatId::eJobProfile-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_people.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId =15"><!--CTodoCatId::eAssignedTraining-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_learn.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId =26"><!--CTodoCatId::eLearningAlert-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_learn.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId =16"><!--CTodoCatId::eLearningSurvey-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_learn.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId =11"><!--CTodoCatId::eOfferLetter-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_recruit.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId = 5"><!--CTodoCatId::eRecruitingDef-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_recruit.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId =13"><!--CTodoCatId::eRecruitingEvents-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_recruit.png</xsl:text>
            </xsl:when>
            <xsl:when test="$catId =19"><!--CTodoCatId::eEmpProfileComplete-->
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_complete.png</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>ms-appx-web:///Assets/html/images/daya_todo_ico_empty.png</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>