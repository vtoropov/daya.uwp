<?xml version="1.0" encoding="Windows-1252"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:m="urn:1"
    xmlns:d="urn:2"
    version="1.0">

    <xsl:param name ="global-company-id"><xsl:value-of select="'${global_cid_value}'"/></xsl:param>    
    <xsl:param name ="global-user-name" ><xsl:value-of select="'${global_usr_value}'"/></xsl:param>
    <xsl:param name ="global-user-pass" ><xsl:value-of select="'${global_pwd_value}'"/></xsl:param>

    <xsl:import href="_data_common_img_defs.xsl"/>
    <xsl:import href="_data_common_url_defs.xsl"/>
    <xsl:import href="_data_common_task_defs.xsl"/>
    <xsl:import href="_data_common_task_one_step.xsl"/>
    <xsl:import href="_data_common_task_multi_step.xsl"/>

    <xsl:template match="/">
        <xsl:for-each select="feed/entry/content/m:properties">
            <xsl:variable name="id" select="d:categoryId"/>
            <div class="section" id="{$id}" onclick="fn_on_sec_click(this.id)">
               <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <colgroup>
                        <col width="10px"/>
                        <col width="24px"/>
                        <col width="100%"/>
                    </colgroup>
                    <tr>
                        <td><div style="width=10px"></div></td>
                        <td><div class="section_col" id="{$id}_img">
                                           &#160;&#160;
                        </div></td>
                        <td class="section_title">
                            <xsl:value-of select="d:categoryLabel"/> (<xsl:value-of select="count(d:todos/d:element)"/>)
                        </td>
                    </tr>
               </table>
            </div>
            <div id="{$id}_cnt" style="display:none">
                <xsl:apply-templates select="d:todos"/>
            </div>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="d:todos">
        <xsl:for-each select="d:element">
            <xsl:choose>
                <xsl:when test="count(d:entries/d:element) > 0">
                    <xsl:call-template name="multi-step-task">
                       <xsl:with-param name="objId"     select="d:entryId"/>
                       <xsl:with-param name="catId"     select="d:categoryId"/>
                       <xsl:with-param name="title"     select="d:stepDescAlt"/>
                       <xsl:with-param name="overdue"   select="d:dueDateOffSet"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="single-step-task">
                       <xsl:with-param name="objId"     select="d:entryId"/>
                       <xsl:with-param name="catId"     select="d:categoryId"/>
                       <xsl:with-param name="overdue"   select="d:dueDateOffSet"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>