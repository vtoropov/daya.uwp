<?xml version="1.0" encoding="Windows-1252"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:m="urn:1"
    xmlns:d="urn:2"
    version="1.0">

    <xsl:template  name="single-step-task">
        <xsl:param name="objId"  />
        <xsl:param name="catId"  />
        <xsl:param name="overdue"/>
        
        <xsl:variable name="img_src">
            <xsl:call-template name="cat-id-to-image-name">
                <xsl:with-param name="catId" select="$catId"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="over_cls">
            <xsl:call-template name="overdue-days-to-class">
                <xsl:with-param name="over_days" select="$overdue"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="over_txt">
            <xsl:call-template name="overdue-days-to-text">
                <xsl:with-param name="over_days" select="$overdue"/>
            </xsl:call-template>
        </xsl:variable>
        <div id="{$objId}"
            style="height:50px;overflow-y:hidden;">
            <table border="0" cellpadding="5px" cellspacing="0" width="100%" height="100%" class="tsk_list">
                <colgroup>
                    <col width="16px"/>
                    <col width="64px" align="center"/>
                    <col width="32px"/>
                    <col width="100%"/>
                    <col width="32px"/>
                </colgroup>
                <tr valign="middle" height="50px">
                    <td><div style="width:16px"></div></td>
                    <td class="{$over_cls}"><div style="width:70px;"><xsl:value-of select="$over_txt"/></div></td>
                    <td><img src="{$img_src}"/></td>
                    <xsl:choose>
                        <xsl:when test="d:categoryId = 19">
                            <td><a>
                                <xsl:attribute name="href">
                                    <xsl:call-template name="to-do-task-url">
                                        <xsl:with-param name="cat-id" select="d:categoryId"/>
                                    </xsl:call-template>
                                </xsl:attribute>
                                <xsl:value-of select="d:name"/>
                            </a></td>
                        </xsl:when>
                        <xsl:otherwise>
                            <td><a>
                                <xsl:attribute name="href">
                                    <xsl:call-template name="to-do-task-url">
                                        <xsl:with-param name="cat-id" select="d:categoryId"/>
                                    </xsl:call-template>
                                </xsl:attribute>
                                <xsl:value-of select="d:stepDescAlt"/>
                            </a></td>
                        </xsl:otherwise>
                    </xsl:choose>
                    <td></td>
                </tr>
            </table>
        </div>
    </xsl:template>

</xsl:stylesheet>