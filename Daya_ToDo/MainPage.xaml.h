﻿#ifndef _DAYAMAINPAGEXAML_H_783C8846_E3B5_4DCD_AEC7_06E0379A1649_INCLUDED
#define _DAYAMAINPAGEXAML_H_783C8846_E3B5_4DCD_AEC7_06E0379A1649_INCLUDED
//
// Created by Tech_dog (VToropov) on 19-Oct-2016 at 9:58:58p, GMT+7, Phuket, Rawai, Wednesday;
// This is Daya Dimensi Global UWP application main page class declaration file.
//
#include "UserProfile.h"
#include "MainPage.g.h"
#include "MainPage.StatusBar.h"

#include "HttpDataProvider.h"
#include "HtmlDataProvider.h"
#include "XmlDataProvider.h"

#include "Daya_ToDo_NotifyMan.h"

namespace Daya_ToDo
{
	using Daya_ToDo::CSysError;
	using Daya_ToDo::Data::CHttpDataProvider;
	using Daya_ToDo::Data::CHtmlDataProvider;
	using Daya_ToDo::Data::IGenericProviderSink;
	using Daya_ToDo::Data::eOperation;
	using Daya_ToDo::Data::CUserProfile;
	using Daya_ToDo::Data::Xml::CXmlDataProvider;

	typedef Windows::UI::Xaml::Controls::Frame TFrame;

	class MainPageEventHandler : public IGenericProviderSink
	{
	private: // IGenericProviderSink
		VOID IGenericProviderSink_OnBegin (const eOperation) override sealed;
		VOID IGenericProviderSink_OnError (const CSysError ) override sealed;
		VOID IGenericProviderSink_OnFinish(const eOperation) override sealed;
	};

	/// <summary>
	/// This is the main page of the application. It shows the web control and status/message bar;
	/// </summary>
	public ref class MainPage sealed
	{
		friend class MainPageEventHandler;
		friend ref class MainPageStatus;
	internal:
		static
		CUserProfile           m_profile;
		static bool            m_profileUpdated;
	private:
		MainPageEventHandler   m_evt_handler;
		CHttpDataProvider      m_web_provider;
		CHtmlDataProvider      m_htm_provider;
		CXmlDataProvider       m_xml_provider;
	private:
		notify::CNotifyManager m_notify_man;
	public:
		MainPage(void);
	public:
		static MainPage^       Instance(void);
		static MainPageStatus^ Status(void);
		static VOID            SetStartFromAC(bool _value);
	public:
		VOID FireDataUpdate(void);
		VOID ShowProgress(bool _show);
	protected:
		VOID OnNavigatedFrom(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override sealed;
		VOID OnNavigatedTo  (Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override sealed;
		VOID OnSizeChanged  (Platform::Object^ _sender, Windows::UI::Core::WindowSizeChangedEventArgs^ _args);
	protected:
		VOID Web_NavigationCompleted(Windows::UI::Xaml::Controls::WebView^ _sender, Windows::UI::Xaml::Controls::WebViewNavigationCompletedEventArgs^ _args);
		VOID Web_NavigationStarted(Windows::UI::Xaml::Controls::WebView^ _sender, Windows::UI::Xaml::Controls::WebViewNavigationStartingEventArgs^ _args);
		VOID Web_OnScriptNotify(Platform::Object^ _sender, Windows::UI::Xaml::Controls::NotifyEventArgs^ _args);
	};
}

#endif/*_DAYAMAINPAGEXAML_H_783C8846_E3B5_4DCD_AEC7_06E0379A1649_INCLUDED*/