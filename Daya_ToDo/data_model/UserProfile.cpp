/*
	Created by Tech_dog (VToropov) on 24-Oct-2016 at 9:17:32p, UTC+7, Phuket, Rawai, Monday;
	This is Daya Dimensi Global UWP application user profile class implementation file.
*/
#include "pch.h"
#include "UserProfile.h"

using namespace Daya_ToDo;
using namespace Daya_ToDo::Data;

using namespace Windows::Security::Cryptography;

/////////////////////////////////////////////////////////////////////////////

CUserProfile::CUserProfile(void)
{
}

/////////////////////////////////////////////////////////////////////////////

Platform::String^ CUserProfile::AuthenticateSeq(void)const
{
	Platform::String^ auth_ = ref new Platform::String();
	auth_ += ref new Platform::String(this->UserName().c_str());
	auth_ += "@";
	auth_ += ref new Platform::String(this->CoIdentifier().c_str());
	auth_ += ":";
	auth_ += ref new Platform::String(this->Password().c_str());

	auth_ = CryptographicBuffer::EncodeToBase64String(
		CryptographicBuffer::ConvertStringToBinary(
			auth_,
			BinaryStringEncoding::Utf8
		));
	return auth_;
}

VOID            CUserProfile::Clear(void)
{
	if (!m_coId.empty())m_coId.clear();
	if (!m_user.empty())m_user.clear();
	if (!m_pass.empty())m_pass.clear();
}

std::wstring    CUserProfile::CoIdentifier(void)const
{
	return m_coId;
}

HRESULT         CUserProfile::CoIdentifier(LPCTSTR lpszId)
{
	if (!::wcslen(lpszId))
		return E_INVALIDARG;
	m_coId = lpszId;
	HRESULT hr_ = S_OK;
	return  hr_;
}

bool            CUserProfile::IsValid(void)const
{
	return (!this->m_coId.empty() && !this->m_pass.empty() && !this->m_user.empty());
}

std::wstring    CUserProfile::Password(void)const
{
	return m_pass;
}

HRESULT         CUserProfile::Password(LPCTSTR lpszPwd)
{
	if (!::wcslen(lpszPwd))
		return E_INVALIDARG;
	m_pass = lpszPwd;
	HRESULT hr_ = S_OK;
	return  hr_;
}

std::wstring    CUserProfile::UserName(void)const
{
	return m_user;
}

HRESULT         CUserProfile::UserName(LPCTSTR lpszUser)
{
	if (!::wcslen(lpszUser))
		return E_INVALIDARG;
	m_user = lpszUser;
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

using namespace Windows::Storage;
using namespace Windows::Foundation;

/////////////////////////////////////////////////////////////////////////////

CUserProfilePersistent::CUserProfilePersistent(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CUserProfilePersistent::Load(void)
{
	ApplicationDataContainer^ cfg_ = ApplicationData::Current->LocalSettings;
	auto values_ = cfg_->Values;

	LPCTSTR lpszKeys[] = {
		_T("coId"), _T("user"), _T("pass")
	};

	for (INT i_ = 0; i_ < _countof(lpszKeys); i_++)
	{
		try
		{
			Platform::String^ value_ = safe_cast<Platform::String^>(values_->Lookup(ref new Platform::String(lpszKeys[i_])));
			if (!value_)
				continue;
			switch (i_)
			{
			case 0: TBase::m_coId = value_->Data(); break;
			case 1: TBase::m_user = value_->Data(); break;
			case 2: TBase::m_pass = value_->Data(); break;
			default:
				break;
			}
		}
		catch (Platform::COMException^ e)
		{
		}
	}

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT         CUserProfilePersistent::Save(void)
{
	ApplicationDataContainer^ cfg_ = ApplicationData::Current->LocalSettings;
	auto values_ = cfg_->Values;

	LPCTSTR lpszKeys[] = {
		_T("coId"), _T("user"), _T("pass")
	};

	for (INT i_ = 0; i_ < _countof(lpszKeys); i_++)
	{
		Platform::String^ value_ = nullptr;

		switch (i_)
		{
		case 0: value_ = ref new Platform::String(TBase::m_coId.c_str()); break;
		case 1: value_ = ref new Platform::String(TBase::m_user.c_str()); break;
		case 2: value_ = ref new Platform::String(TBase::m_pass.c_str()); break;
		default:
			continue;
		}

		values_->Insert(
			ref new Platform::String(lpszKeys[i_]),
			dynamic_cast<PropertyValue^>(PropertyValue::CreateString(value_))
		);
	}

	HRESULT hr_ = S_OK;
	return  hr_;
}