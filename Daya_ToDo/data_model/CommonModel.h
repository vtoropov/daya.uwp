#ifndef _DAYACOMMONMODEL_H_87C5C707_945A_4EA8_B74E_58B4C755B84F_INCLUDED
#define _DAYACOMMONMODEL_H_87C5C707_945A_4EA8_B74E_58B4C755B84F_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Oct-2016 at 5:44:59p, UTC+7, Phuket, Rawai, Monday;
	This is Daya Dimensi Global UWP application common data model class(es) declaration file.
*/
namespace Daya_ToDo
{
		class CSysError
		{
		private:
			HRESULT       m_result;
			std::wstring  m_details;
		public:
			CSysError(void);
			CSysError(Platform::Exception^);
			CSysError(const ::std::exception&);
		public:
			LPCTSTR       GetDetails(void) const;                           // gets error description
			std::wstring  GetFormattedDetails(const bool bMultiline=false)const; // gets formatted string like this: code, description
			HRESULT       GetHresult(void) const;                           // gets error code
			VOID          SetStatus(const HRESULT, LPCTSTR lpszDetails);    // sets error object state
		public:
			CSysError& operator= (const HRESULT);
			CSysError& operator= (const ::std::exception&);
			CSysError& operator= (Platform::Exception^);                    // sets error object state from platform exception object
		public:
			operator bool(void)const;
			operator HRESULT(void) const;
		};

namespace Data
{
		public enum class eOperation
		{
			eOperUndefined            = 0x00,
			eOperFailure              = 0x01,
			eOperHttpQueryStarted     = 0x02,
			eOperHttpQueryFinished    = 0x03,
			eOperXmlParsingStarted    = 0x04,
			eOperXmlParsingFinished   = 0x05,
			eOperHtmlInitStarted      = 0x06,
			eOperHtmlInitFinished     = 0x07,
			eOperHtmlOpenStarted      = 0x08,
			eOperHtmlOpenFinished     = 0x09,
			eOperXslLinkInitStarted   = 0x0a,
			eOperXslLinkInitFinished  = 0x0b,
			eOperXslCacheInitStarted  = 0x0c,
			eOperXslCacheInitFinished = 0x0d,
			eOperNotifyInitStarted    = 0x0e,
			eOperNotifyInitFinished   = 0x0f,
		};

		interface IGenericProviderSink
		{
			virtual VOID IGenericProviderSink_OnBegin (const eOperation) PURE;
			virtual VOID IGenericProviderSink_OnError (const CSysError ) PURE;
			virtual VOID IGenericProviderSink_OnFinish(const eOperation) PURE;
		};
	}
}

typedef const Daya_ToDo::CSysError& TErrorRef;

#endif/*_DAYACOMMONMODEL_H_87C5C707_945A_4EA8_B74E_58B4C755B84F_INCLUDED*/
