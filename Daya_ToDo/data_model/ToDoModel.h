#ifndef _DAYATODOMODEL_H_FCCEE3B9_2A55_46DE_A3B3_82DBDFDAC9C4_INCLUDED
#define _DAYATODOMODEL_H_FCCEE3B9_2A55_46DE_A3B3_82DBDFDAC9C4_INCLUDED
/*
	Created by Tech_dog (VToropov) on 30-Oct-2016 at 9:52:41p, UTC+7, Phuket, Rawai, Sunday;
	This is Daya Dimensi Global UWP application ToDo data model class(es) declaration file.
*/

namespace Daya_ToDo
{
	namespace Data
	{
		class CTodoStatus
		{
		public:
			enum _Id {              // file://SuccessFactors_HCM_Suite_OData_API_Reference_en.pdf#page_121
				eUnknown   = 0x0,
				eUpcoming  = 0x1,   // Upcoming task
				eActive    = 0x2,   // Active task which is pending for logged in user to take action
				eCompleted = 0x3,   // Completed task.
			};
		private:
			_Id            m_id;
			std::wstring   m_name;
		public:
			CTodoStatus(void);
		public:
			_Id            Identifier(void)const;
			VOID           Identifier(const INT);
			std::wstring   Name(void)const;
			VOID           Name(LPCTSTR);
		};
	}
}

#endif/*_DAYATODOMODEL_H_FCCEE3B9_2A55_46DE_A3B3_82DBDFDAC9C4_INCLUDED*/