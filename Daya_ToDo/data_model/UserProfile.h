#ifndef _DAYAUSERPROFILE_H_A06E6E81_8A74_4BC2_94A6_3062E725E90A_INCLUDED
#define _DAYAUSERPROFILE_H_A06E6E81_8A74_4BC2_94A6_3062E725E90A_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Oct-2016 at 9:10:39p, UTC+7, Phuket, Rawai, Monday;
	This is Daya Dimensi Global UWP application user profile class declaration file.
*/
#include "CommonModel.h"

namespace Daya_ToDo {
	namespace Data {
		class CUserProfile
		{
		protected:
			std::wstring    m_coId;     // company identifier
			std::wstring    m_user;     // user name
			std::wstring    m_pass;     // password (plain text)
		public:
			CUserProfile(void);
		public:
			Platform::String^ AuthenticateSeq(void)const;
			VOID            Clear       (void);
			std::wstring    CoIdentifier(void)const;
			HRESULT         CoIdentifier(LPCTSTR);
			bool            IsValid     (void)const;
			std::wstring    Password    (void)const;
			HRESULT         Password    (LPCTSTR);
			std::wstring    UserName    (void)const;
			HRESULT         UserName    (LPCTSTR);
		};

		class CUserProfilePersistent : public CUserProfile
		{
			typedef CUserProfile TBase;
		public:
			CUserProfilePersistent(void);
		public:
			HRESULT         Load(void);
			HRESULT         Save(void);
		};
	}
}

#endif/*_DAYAUSERPROFILE_H_A06E6E81_8A74_4BC2_94A6_3062E725E90A_INCLUDED*/