/*
	Created by Tech_dog (VToropov) on 30-Oct-2016 at 9:58:21p, UTC+7, Phuket, Rawai, Sunday;
	This is Daya Dimensi Global UWP application ToDo data model class(es) implementation file.
*/
#include "pch.h"
#include "ToDoModel.h"

using namespace Daya_ToDo;
using namespace Daya_ToDo::Data;

/////////////////////////////////////////////////////////////////////////////

CTodoStatus::CTodoStatus(void) : m_id(CTodoStatus::eUnknown)
{
}

/////////////////////////////////////////////////////////////////////////////

CTodoStatus::_Id CTodoStatus::Identifier(void)const
{
	return m_id;
}

VOID           CTodoStatus::Identifier(const INT _id)
{
	switch (_id)
	{
	case 1:
	case 2:
	{
		m_id = static_cast<CTodoStatus::_Id>(_id);
	} break;
	default:
		m_id = CTodoStatus::eUnknown;
	}
}

std::wstring   CTodoStatus::Name(void)const
{
	return m_name;
}

VOID           CTodoStatus::Name(LPCTSTR lpszName)
{
	m_name = lpszName;
}