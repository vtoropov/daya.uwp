/*
	Created by Tech_dog (VToropov) on 24-Oct-2016 at 5:57:26p, UTC+7, Phuket, Rawai, Monday;
	This is Daya Dimensi Global UWP application HTML data provider class implementation file.
*/
#include "pch.h"
#include "HtmlDataProvider.h"

using namespace Daya_ToDo;
using namespace Daya_ToDo::Data;

#include "XmlDataProvider.h"

using namespace Daya_ToDo::Data::Xml;

#include <ppltasks.h> 
using namespace concurrency;

using namespace Windows::Foundation;
using namespace Windows::Storage;

static std::wstring content_;    // html to display

using namespace Platform;
using namespace Platform::Collections;
/////////////////////////////////////////////////////////////////////////////

CHtmlFilter::CHtmlFilter(void) : m_dwState(CHtmlFilter::eAllTask), m_eSortOrder(CHtmlFilter::eSortByType)
{
}

/////////////////////////////////////////////////////////////////////////////

CHtmlFilter::_SortOrder
CHtmlFilter::SortOrder(void)const
{
	return m_eSortOrder;
}

VOID     CHtmlFilter::SortOrder(const CHtmlFilter::_SortOrder _order)
{
	m_eSortOrder = _order;
}

DWORD    CHtmlFilter::State(void)const
{
	return m_dwState;
}

VOID     CHtmlFilter::State(const DWORD _state)
{
	m_dwState = _state;
}

/////////////////////////////////////////////////////////////////////////////

CHtmlFilterEx::CHtmlFilterEx(void) : TBase()
{
}

CHtmlFilterEx::CHtmlFilterEx(const CHtmlFilter& _filter): TBase()
{
	TBase::m_dwState = _filter.State();
	TBase::m_eSortOrder = _filter.SortOrder();
}

/////////////////////////////////////////////////////////////////////////////

std::wstring   CHtmlFilterEx::GetXslTemplateName(void)const
{
	const CHtmlFilter::_SortOrder order_ = TBase::SortOrder();
	const DWORD dwStatus = TBase::State();

	std::wstring name_;// = _T("ms-appx:///Assets/xslt/");

	if (CHtmlFilter::eSortByDate == order_ && CTodoStatus::eActive  == dwStatus) name_ = _T("_data_sorted_by_dat_act.xsl");
	if (CHtmlFilter::eSortByType == order_ && CTodoStatus::eActive  == dwStatus) name_ = _T("_data_sorted_by_cat_act.xsl");
	if (CHtmlFilter::eSortByDate == order_ && CHtmlFilter::eAllTask == dwStatus) name_ = _T("_data_sorted_by_dat_all.xsl");
	if (CHtmlFilter::eSortByType == order_ && CHtmlFilter::eAllTask == dwStatus) name_ = _T("_data_sorted_by_cat_all.xsl");

	_ASSERT(!name_.empty());

	return name_;
}

bool           CHtmlFilterEx::UpdateFromCommand(Platform::String^ _cmd)
{
	bool bChanged = false;
	if (!bChanged && "cmd_tsk_sort_lst_by_date" == _cmd)
	{
		bChanged = (CHtmlFilter::eSortByDate != m_eSortOrder);
		if (bChanged)
			m_eSortOrder = CHtmlFilter::eSortByDate;
	}
	if (!bChanged && "cmd_tsk_sort_lst_by_type" == _cmd)
	{
		bChanged = (CHtmlFilter::eSortByType != m_eSortOrder);
		if (bChanged)
			m_eSortOrder = CHtmlFilter::eSortByType;
	}
	if (!bChanged && "cmd_tsk_show_active_only" == _cmd)
	{
		bChanged = (CTodoStatus::eActive != m_dwState);
		if (bChanged)
			m_dwState = CTodoStatus::eActive;
	}
	if (!bChanged && "cmd_tsk_show_active_upcome" == _cmd)
	{
		bChanged = (CHtmlFilter::eAllTask != m_dwState);
		if (bChanged)
			m_dwState = CHtmlFilter::eAllTask;
	}
	return bChanged;
}

/////////////////////////////////////////////////////////////////////////////

CHtmlFilterEx& CHtmlFilterEx::operator=(const CHtmlFilter& _filter)
{
	TBase::m_dwState = _filter.State();
	TBase::m_eSortOrder = _filter.SortOrder();
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CHtmlStyle::CHtmlStyle(void)
{
	m_body_style = ref new Platform::String();
	m_body_style+= "function _a{alert('__fuck__!');}";
}

/////////////////////////////////////////////////////////////////////////////

VOID      CHtmlStyle::ApplyTo(::Windows::UI::Xaml::Controls::WebView^ _web_ctrl)const
{
	Platform::Array<Platform::String^>^ args_ = {m_body_style};
	Platform::Collections::Vector<String^>^ vec_a = ref new Vector<String^>(args_);

	if (_web_ctrl != nullptr)
		_web_ctrl->InvokeScriptAsync("eval", vec_a);
}

/////////////////////////////////////////////////////////////////////////////

namespace Daya_ToDo {
	namespace Data {
		namespace details
		{
			VOID CHtmlDataProvider_ApplyFilter(const CHtmlFilterEx& _filter, ::std::wstring& _html)
			{
				static LPCTSTR lpszPlaceholder[] = {
					_T("${selected_active}"),
					_T("${selected_all}")   ,
					_T("${sort_by_date}")   ,
					_T("${sort_by_type}")
				};

				for (INT i_ = 0; i_ < _countof(lpszPlaceholder); i_++)
				{
					std::wstring::size_type pos_ = _html.find(lpszPlaceholder[i_]);
					if (std::wstring::npos == pos_)
						continue;

					::std::wstring replacement_;

					switch (i_)
					{
					case 0: replacement_ = (CHtmlFilter::eAllTask == _filter.State() ? _T("") : _T("selected")); break;
					case 1: replacement_ = (CHtmlFilter::eAllTask != _filter.State() ? _T("") : _T("selected")); break;
					case 2: replacement_ = (
							CHtmlFilter::eSortByDate == _filter.SortOrder() ?
							_T("Date") :
							_T("<a href=\"http://command/cmd_tsk_sort_lst_by_date\" target=\"_self\">Date</a>")
						); break;
					case 3: replacement_ = (
							CHtmlFilter::eSortByDate != _filter.SortOrder() ?
							_T("Type") :
							_T("<a href=\"http://command/cmd_tsk_sort_lst_by_type\" target=\"_self\">Type</a>")
						); break;
					}
					_html.replace(
						pos_, ::wcslen(lpszPlaceholder[i_]), replacement_
					);
				}
			}

			VOID CHtmlDataProvider_InjectData(const ::std::wstring& _html, ::std::wstring& _dyna_data)
			{
				LPCTSTR lpszPlaceholder = _T("<!--{$dyna_html_placeholder}-->");
				std::wstring::size_type pos_ = _html.find(lpszPlaceholder);
				if (std::wstring::npos == pos_)
					return;
				std::wstring processed_ = _html;
				processed_.replace(
						pos_, ::wcslen(lpszPlaceholder), _dyna_data
					);
				_dyna_data = processed_;
			}
		}
	}
}

THtmlCache   CHtmlDataProvider::m_cache;
eOperation   CHtmlDataProvider::m_operate = eOperation::eOperUndefined;
/////////////////////////////////////////////////////////////////////////////

CHtmlDataProvider::CHtmlDataProvider(IGenericProviderSink& _sink) : m_sink(_sink)
{}

/////////////////////////////////////////////////////////////////////////////

Platform::String^  CHtmlDataProvider::CommandFromUrl(Platform::String^ _URL) const
{
	Platform::String^ url_ = nullptr;
	if (_URL != nullptr)
		if (!_URL->IsEmpty())
		{
			std::wstring w_url_(_URL->Data());
			std::wstring::size_type pos_ = w_url_.find(_T("command"));
			if (std::wstring::npos != pos_)
			{
				w_url_.replace(0, ::wcslen(_T("http://command/")), _T(""));
				url_ = ref new Platform::String(w_url_.c_str());
			}
		}
	return url_;
}

Platform::String^  CHtmlDataProvider::Content(void)const
{
	return ref new Platform::String(content_.c_str());
}

HRESULT            CHtmlDataProvider::CreateContentAsync(const CHtmlFilterEx& _filter, const CUserProfile& _profile, const std::wstring& _raw_xml)
{
	if (eOperation::eOperHtmlOpenStarted == CHtmlDataProvider::m_operate)
		return S_OK;

	CHtmlDataProvider::m_operate = eOperation::eOperHtmlOpenStarted;
	m_error  = S_OK;
	m_sink.IGenericProviderSink_OnBegin(CHtmlDataProvider::m_operate);

	task_ext::run_async_non_interactive([this, _filter, _profile, _raw_xml]()
	{
		::std::wstring xsl_name_ = _filter.GetXslTemplateName();
		TXmlQryParams params_ = CXmlDataQuery::Prepare(_profile);
		CXmlDataProvider provider_(m_sink);

		::std::wstring raw_xsl_ = provider_.XslCache().FileContent(xsl_name_.c_str());
		try
		{
			HRESULT hr_ = provider_.Transform(_raw_xml, raw_xsl_, params_);
			if (FAILED(hr_))
				m_error = provider_.Error();
			if (m_error)
			{
				CHtmlDataProvider::m_operate = eOperation::eOperFailure;
				m_sink.IGenericProviderSink_OnError(m_error);
			}
			else
			{
				::std::wstring page_ = _T("");
				THtmlCache::const_iterator it_ = CHtmlDataProvider::m_cache.find(_T("daya_todo_list.html"));
				if (it_ != CHtmlDataProvider::m_cache.end())
					page_ = it_->second;

				details::CHtmlDataProvider_ApplyFilter(_filter, page_);

				content_ = provider_.Result();
				details::CHtmlDataProvider_InjectData(
					page_,
					content_
				);
				CHtmlDataProvider::m_operate = eOperation::eOperHtmlOpenFinished;
				m_sink.IGenericProviderSink_OnFinish(CHtmlDataProvider::m_operate);
			}
		}
		catch (Platform::Exception^ _ex)
		{
			m_error = _ex;
			m_sink.IGenericProviderSink_OnError(m_error);
			CHtmlDataProvider::m_operate = eOperation::eOperFailure;
		}
	});

	return m_error;
}

std::wstring       CHtmlDataProvider::DefaultPage(void)const
{
	::std::wstring default_ = _T("");
	THtmlCache::const_iterator it_ = CHtmlDataProvider::m_cache.find(_T("daya_todo_default.html"));
	if (it_ != CHtmlDataProvider::m_cache.end())
		default_ = it_->second;
	return default_;
}

TErrorRef          CHtmlDataProvider::Error(void) const
{
	return m_error;
}

bool               CHtmlDataProvider::IsContentAvailable(void)const
{
	return (content_.empty() == false);
}

CHtmlStyle&        CHtmlDataProvider::Style(void)
{
	return m_style;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT            CHtmlDataProvider::InitializeAsync(void)
{
	m_error = S_OK;
	if (CHtmlDataProvider::m_cache.empty() == false)
		return m_error;

	m_sink.IGenericProviderSink_OnBegin(eOperation::eOperHtmlInitStarted);

	static ::std::map<std::wstring, std::wstring> files_;
	if (files_.empty())
	{
		files_.insert(
			::std::make_pair(_T("daya_todo_default.html"), _T("ms-appx:///Assets/html/daya_todo_default.html"))
		);
		files_.insert(
			::std::make_pair(_T("daya_todo_list.html"), _T("ms-appx:///Assets/html/daya_todo_list.html"))
		);
	}
	static size_t counter_ = 0;

	std::for_each(
		::std::begin(files_), ::std::end(files_), [this](::std::pair<std::wstring, std::wstring> _file)
	{
		Uri^ uri_ = ref new Uri(
			ref new Platform::String(_file.second.c_str())
		);
		create_task(StorageFile::GetFileFromApplicationUriAsync(uri_))
			.then([this](task<StorageFile^> file_obj)
		{
			return create_task(Windows::Storage::FileIO::ReadTextAsync(file_obj.get()));
		},
				task_continuation_context::use_current())
			.then([this, _file](task<Platform::String^> _raw_html)
		{
			try
			{
				::std::wstring content_ = _raw_html.get()->Data();

				CHtmlDataProvider::m_cache.insert(
					::std::make_pair(_file.first, content_)
				);

				if (++counter_ == files_.size())
					m_sink.IGenericProviderSink_OnFinish(eOperation::eOperHtmlInitFinished);
			}
			catch (::std::bad_alloc&)
			{
				m_error.SetStatus(
					E_OUTOFMEMORY,
					_T("Cannot initialize HTML page cache")
				);
				m_sink.IGenericProviderSink_OnError(m_error);
			}
		});
	});
	return m_error;
}