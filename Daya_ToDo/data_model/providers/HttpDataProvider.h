#ifndef _DAYAHTTPDATAPROVIDER_H_3470F131_791E_40AE_894D_0A8DA30EB56F_INCLUDED
#define _DAYAHTTPDATAPROVIDER_H_3470F131_791E_40AE_894D_0A8DA30EB56F_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Oct-2016 at 8:07:38p, UTC+7, Phuket, Rawai, Monday;
	This is Daya Dimensi Global UWP application HTTP data provider class declaration file.
*/
#include "UserProfile.h"
#include "CommonModel.h"

namespace Daya_ToDo {
	namespace Data {

		class CHttpDataProvider
		{
		private:
			bool             m_busy;  // if true: retrieving data is in progress
			CSysError        m_error;
		private:
			IGenericProviderSink& m_sink;
		public:
			CHttpDataProvider(IGenericProviderSink&);
			~CHttpDataProvider(void);
		public:
			TErrorRef        Error(void)const;
			bool             IsBusy(void)const;
			HRESULT          RetrieveData(const CUserProfile&);
		public:
			static const std::wstring& Cache(void);
		};
	}
}
#endif/*_DAYAHTTPDATAPROVIDER_H_3470F131_791E_40AE_894D_0A8DA30EB56F_INCLUDED*/