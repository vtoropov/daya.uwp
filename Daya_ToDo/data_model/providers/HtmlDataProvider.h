#ifndef _DAYAHTMLDATAPROVIDER_H_64761A8C_5D19_4A4D_92B9_B30B58EB7220_INCLUDED
#define _DAYAHTMLDATAPROVIDER_H_64761A8C_5D19_4A4D_92B9_B30B58EB7220_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Oct-2016 at 3:47:22p, UTC+7, Phuket, Rawai, Monday;
	This is Daya Dimensi Global UWP application HTML data provider class declaration file.
*/
#include "CommonModel.h"
#include "ToDoModel.h"
#include "UserProfile.h"

namespace Daya_ToDo { namespace Data
{
	class CHtmlFilter
	{
	public:
		enum {
			eAllTask = CTodoStatus::eUpcoming | CTodoStatus::eActive
		};
	public:
		enum _SortOrder {
			eSortByType = 0, // default
			eSortByDate = 1,
		};
	protected:
		DWORD          m_dwState;    // bit-mask for task status
		_SortOrder     m_eSortOrder;
	public:
		CHtmlFilter(void);
	public:
		_SortOrder     SortOrder(void)const;
		VOID           SortOrder(const _SortOrder);
		DWORD          State(void)const;
		VOID           State(const DWORD);
	};

	class CHtmlFilterEx : public CHtmlFilter
	{
		typedef CHtmlFilter TBase;
	public:
		CHtmlFilterEx(void);
		CHtmlFilterEx(const CHtmlFilter&);
	public:
		std::wstring   GetXslTemplateName(void)const;
		bool           UpdateFromCommand(Platform::String^);  // returns true if filter settings are changed;
	public:
		CHtmlFilterEx& operator=(const CHtmlFilter&);
	};

	class CHtmlStyle
	{
	private:
		Platform::String^  m_body_style;
	public:
		CHtmlStyle(void);
	public:
		VOID      ApplyTo(::Windows::UI::Xaml::Controls::WebView^ _web_ctrl) const;
	};

	typedef ::std::map<::std::wstring, ::std::wstring> THtmlCache; // first - name; second - content;

	class CHtmlDataProvider
	{
	private:
		IGenericProviderSink&
		                   m_sink;
		CSysError          m_error;
		CHtmlStyle         m_style;
	private:
		static
		THtmlCache         m_cache;      // includes default html page (without dynamic data) and main html page for composing final UI
		static
		eOperation         m_operate;    // current state of an operation that is being performed
	public:
		CHtmlDataProvider(IGenericProviderSink&);
	public:
		Platform::String^  CommandFromUrl(Platform::String^ _URL) const;
		Platform::String^  Content(void)const;            // returns dynamically created content;
		HRESULT            CreateContentAsync(const CHtmlFilterEx&, const CUserProfile&, const std::wstring& _raw_xml);
		std::wstring       DefaultPage(void)const;
		TErrorRef          Error(void) const;
		bool               IsContentAvailable(void)const; // checks if dynamic content is available;
		CHtmlStyle&        Style(void);
	public:
		HRESULT            InitializeAsync(void); // initializes HTML page cache
	};
}}

#endif/*_DAYAHTMLDATAPROVIDER_H_64761A8C_5D19_4A4D_92B9_B30B58EB7220_INCLUDED*/