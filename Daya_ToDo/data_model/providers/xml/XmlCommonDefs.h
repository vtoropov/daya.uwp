#ifndef _DAYAXMLCOMMONDEFS_H_C1F5FA6B_2D1B_408E_A1EC_1EDC4DF6E558_INCLUDED
#define _DAYAXMLCOMMONDEFS_H_C1F5FA6B_2D1B_408E_A1EC_1EDC4DF6E558_INCLUDED
/*
	Created by Tech_dog (VToropov) on 6-Nov-2016 at 6:26:36p, UTC+7, Phuket, Rawai, Sunday;
	This is Daya Dimensi Global UWP application common XML/XSL data conversion definition file.
*/
#include "CommonModel.h"

namespace Daya_ToDo {
	namespace Data {
		namespace Xml
		{
			typedef ::std::map <std::wstring, std::wstring> TXslCache;  // first - name; second - content;
		}
	}
}

#endif/*_DAYAXMLCOMMONDEFS_H_C1F5FA6B_2D1B_408E_A1EC_1EDC4DF6E558_INCLUDED*/