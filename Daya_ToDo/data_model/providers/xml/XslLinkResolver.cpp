/*
	Created by Tech_dog (VToropov) on 2-Nov-2016 at 11:04:01p, GMT+7, Phuket, Rawai, Wednesday;
	This is Daya Dimensi Global UWP application XSL link resolver class implementation file.
*/
#include "pch.h"
#include "XslLinkResolver.h"

using namespace Daya_ToDo;
using namespace Daya_ToDo::Data;
using namespace Daya_ToDo::Data::Xml;

#include <ppltasks.h> 
using namespace concurrency;

using namespace Windows::Foundation;
using namespace Windows::Storage;

#include <sstream>
/////////////////////////////////////////////////////////////////////////////

CXslLink::CXslLink(void) : m_processed(false)
{
}

CXslLink::CXslLink(LPCTSTR lpszName, LPCTSTR lpszPath) : m_processed(false), m_name(lpszName), m_path(lpszPath)
{}

/////////////////////////////////////////////////////////////////////////////

std::wstring   CXslLink::Name(void)const
{
	return m_name;
}

VOID           CXslLink::Name(const ::std::wstring& _name)
{
	m_name = _name;
}

std::wstring   CXslLink::Path(void)const
{
	return m_path;
}

VOID           CXslLink::Path(const ::std::wstring& _path)
{
	m_path = _path;
}

bool           CXslLink::Processed(void)const
{
	return m_processed;
}

VOID           CXslLink::Processed(const bool _proc)
{
	m_processed = _proc;
}

/////////////////////////////////////////////////////////////////////////////

bool CXslLink::operator!=(const CXslLink& _lnk)const
{
	return (this->m_name != _lnk.m_name);
}

bool CXslLink::operator==(const CXslLink& _lnk)const
{
	return (this->m_name == _lnk.m_name);
}

/////////////////////////////////////////////////////////////////////////////

namespace Daya_ToDo {
	namespace Data {
		namespace Xml {
			namespace details
			{
				VOID  CXslLinkResolver_RemoveXslDocNode(std::wstring& _raw_xsl)
				{
					static LPCTSTR lpszTags[] = {
						_T("<xsl:stylesheet"),
						_T("</xsl:stylesheet"),
					};
					std::wstring::size_type pos_ = _raw_xsl.find(lpszTags[0]);
					if (std::wstring::npos != pos_)
					{
						pos_ = _raw_xsl.find(_T(">"), pos_);
						if (std::wstring::npos != pos_)
							_raw_xsl.replace(0, ++pos_, _T(""));
					}
					pos_ = _raw_xsl.rfind(lpszTags[1]);
					if (std::wstring::npos != pos_)
						_raw_xsl.replace(pos_, ::wcslen(lpszTags[1]) + 1, _T(""));
				}

				VOID  CXslLinkResolver_ResolveImport(LPCTSTR lpszLink, const ::std::wstring& lpszReferredXsl, std::wstring& _xsl)
				{
					std::wstring import_ = _T("<xsl:import href=\"%s\"/>");
					std::wstring::size_type pos_ = import_.find(_T("%s"));

					import_.replace(pos_, ::wcslen(_T("%s")), lpszLink);

					pos_ = _xsl.find(import_);
					if (std::wstring::npos == pos_)
						return;
					_xsl.replace(pos_, ::wcslen(import_.c_str()), lpszReferredXsl);
				}
			}
		}
	}
}

TXslCache CXslLinkResolver::m_lnk_cache;
/////////////////////////////////////////////////////////////////////////////

CXslLinkResolver::CXslLinkResolver(IGenericProviderSink& _sink) : m_sink(_sink)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef      CXslLinkResolver::Error(void)const
{
	return m_error;
}

void           CXslLinkResolver::InitializeXslCacheAsync(void)
{
	m_error = S_OK;
	if (this->IsInitialized())
	{
		m_sink.IGenericProviderSink_OnFinish(eOperation::eOperXslLinkInitFinished);
		return;
	}

	this->m_sink.IGenericProviderSink_OnBegin(eOperation::eOperXslLinkInitStarted);

	static TXslLinks links_ = CXslLinkResolver::GetLinks();

	std::for_each(
		::std::begin(links_), ::std::end(links_), [this](CXslLink& _link)
	{
		Uri^ uri_ = ref new Uri(
			ref new Platform::String(_link.Path().c_str())
		);

		create_task(StorageFile::GetFileFromApplicationUriAsync(uri_))
			.then([this](task<StorageFile^> file_obj)
		{
			return create_task(Windows::Storage::FileIO::ReadTextAsync(file_obj.get()));
		},
				task_continuation_context::use_current())
			.then([this, _link](task<Platform::String^> _raw_xsl)
		{
			for (size_t i_ = 0; i_ < links_.size(); i_++)
				if (links_[i_] == _link)
					links_[i_].Processed(true);

			try
			{
				::std::wstring ref_ = _link.Name();
				::std::wstring content_ = _raw_xsl.get()->Data();

				details::CXslLinkResolver_RemoveXslDocNode(content_);

				CXslLinkResolver::m_lnk_cache.insert(
					::std::make_pair(ref_, content_)
				);
				bool bWaitingProcessing = false;

				for (TXslLinks::const_iterator it_ = links_.begin(); it_ != links_.end(); ++it_)
					if (!it_->Processed())
					{
						bWaitingProcessing = true;
						break;
					}
				if (!bWaitingProcessing)
					m_sink.IGenericProviderSink_OnFinish(eOperation::eOperXslLinkInitFinished);
			}
			catch (::std::bad_alloc&)
			{
				m_error.SetStatus(
					E_OUTOFMEMORY,
					_T("Cannot initialize XSL reference cache")
				);
				m_sink.IGenericProviderSink_OnError(m_error);
			}
		});
	});
}

bool           CXslLinkResolver::IsInitialized(void)const
{
	return (CXslLinkResolver::m_lnk_cache.empty() == false);
}

HRESULT        CXslLinkResolver::ResolveImport(::std::wstring& _xsl)
{
	const TXslCache& cache_ = CXslLinkResolver::m_lnk_cache;

	for (TXslCache::const_iterator it_ = cache_.begin(); it_ != cache_.end(); ++it_)
		details::CXslLinkResolver_ResolveImport(
				it_->first.c_str(),
				it_->second,
				_xsl
		);

	m_error = S_OK;
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

TXslLinks      CXslLinkResolver::GetLinks(void)
{
	static LPCTSTR lpszReference[] = {
		_T("_data_common_img_defs.xsl")       ,
		_T("_data_common_url_defs.xsl")       ,
		_T("_data_common_task_defs.xsl")      ,
		_T("_data_common_task_one_step.xsl")  ,
		_T("_data_common_task_multi_step.xsl"),
	};

	TXslLinks links_;

	for (INT i_ = 0; i_ < _countof(lpszReference); i_++)
	{
		std::wstringstream path_;
		path_ << _T("ms-appx:///Assets/xslt/") << lpszReference[i_];

		try
		{
			CXslLink link_(
				lpszReference[i_],
				path_.str().c_str()
			);

			links_.push_back(link_);
		}
		catch (::std::bad_alloc&)
		{
			break;
		}
	}
	return links_;
}