#ifndef _DAYAXSLLINKRESOLVER_H_AA7A4303_3659_47DF_8A2A_BD6718AD59F3_INCLUDED
#define _DAYAXSLLINKRESOLVER_H_AA7A4303_3659_47DF_8A2A_BD6718AD59F3_INCLUDED
/*
	Created by Tech_dog (VToropov) on 2-Nov-2016 at 10:58:48p, GMT+7, Phuket, Rawai, Wednesday;
	This is Daya Dimensi Global UWP application XSL link resolver class declaration file.
*/
#include "XmlCommonDefs.h"

namespace Daya_ToDo {
	namespace Data {
		namespace Xml
		{
			class CXslLink
			{
			private:
				std::wstring     m_path;
				std::wstring     m_name;
				bool             m_processed;
			public:
				CXslLink(void);
				CXslLink(LPCTSTR lpszName, LPCTSTR lpszPath);
			public:
				std::wstring     Name(void)const;
				VOID             Name(const ::std::wstring&);
				std::wstring     Path(void)const;
				VOID             Path(const ::std::wstring&);
				bool             Processed(void)const;
				VOID             Processed(const bool);
			public:
				bool operator!=(const CXslLink&)const;
				bool operator==(const CXslLink&)const;
			};

			typedef ::std::vector<CXslLink>  TXslLinks;

			class CXslLinkResolver
			{
			private:
				static TXslCache m_lnk_cache;    // cache of the XSL files that are dynamically linked/imported to main style sheets
				CSysError        m_error;
			private:
				IGenericProviderSink& m_sink;
			public:
				CXslLinkResolver(IGenericProviderSink&);
			public:
				TErrorRef        Error(void)const;
				void             InitializeXslCacheAsync(void);       // initializes xsl link cache
				bool             IsInitialized(void)const;            // checks an initialization state of this object
				HRESULT          ResolveImport(::std::wstring& _xsl); // resolves specified import URL to resource identifier
			public:
				static TXslLinks GetLinks(void);
			};
		}
	}
}

#endif/*_DAYAXSLLINKRESOLVER_H_AA7A4303_3659_47DF_8A2A_BD6718AD59F3_INCLUDED*/