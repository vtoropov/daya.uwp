#ifndef _DAYAXSLDATACACHE_H_C121859C_D370_4CB0_8D23_2D998C117110_INCLUDED
#define _DAYAXSLDATACACHE_H_C121859C_D370_4CB0_8D23_2D998C117110_INCLUDED
/*
	Created by Tech_dog (VToropov) on 6-Nov-2016 at 6:32:16p, UTC+7, Phuket, Rawai;
	This is Daya Dimensi Global UWP application XSL style sheet cache class declaration file.
*/
#include "XmlCommonDefs.h"

namespace Daya_ToDo {
	namespace Data {
		namespace Xml
		{
			typedef ::std::pair<::std::wstring, ::std::wstring>  TXslFile;
			typedef ::std::map <::std::wstring, ::std::wstring>  TXslEnum; // first - name; second - path;
			class CXslDataCache
			{
			private:
				static TXslCache m_xsl_cache;    // cache of the XSL files that are directly used to transform data (main style sheets)
				CSysError        m_error;
			private:
				IGenericProviderSink& m_sink;
			public:
				CXslDataCache(IGenericProviderSink&);
			public:
				TErrorRef        Error(void)const;
				std::wstring     FileContent(LPCTSTR lpszFileName)const; // gets XSL file content by its name
				void             InitializeXslCacheAsync(void);          // initializes xsl style sheet cache
				bool             IsInitialized(void)const;               // check an initialization state of this object;
			public:
				static
				const TXslEnum   GetPaths(void);
			};
		}
	}
}


#endif/*_DAYAXSLDATACACHE_H_C121859C_D370_4CB0_8D23_2D998C117110_INCLUDED*/