/*
	Created by Tech_dog (VToropov) on 7-Nov-2016 at 4:04:42a, UTC+7, Phuket, Rawai;
	This is Daya Dimensi Global UWP application XSL style sheet cache class implementation file.
*/
#include "pch.h"
#include "XslDataCache.h"

using namespace Daya_ToDo;
using namespace Daya_ToDo::Data;
using namespace Daya_ToDo::Data::Xml;

#include <ppltasks.h> 
using namespace concurrency;

using namespace Windows::Foundation;
using namespace Windows::Storage;

#include <sstream>

TXslCache CXslDataCache::m_xsl_cache;
/////////////////////////////////////////////////////////////////////////////

CXslDataCache::CXslDataCache(IGenericProviderSink& _sink) : m_sink(_sink)
{}

/////////////////////////////////////////////////////////////////////////////

TErrorRef        CXslDataCache::Error(void)const
{
	return m_error;
}

std::wstring     CXslDataCache::FileContent(LPCTSTR lpszFileName)const
{
	TXslEnum::const_iterator it_ = CXslDataCache::m_xsl_cache.find(lpszFileName);
	if (it_ == CXslDataCache::m_xsl_cache.end())
		return _T("");
	return it_->second;
}

void             CXslDataCache::InitializeXslCacheAsync(void)
{
	m_error = S_OK;

	if (this->IsInitialized())
	{
		this->m_sink.IGenericProviderSink_OnFinish(eOperation::eOperXslCacheInitFinished);
		return;
	}

	this->m_sink.IGenericProviderSink_OnBegin(eOperation::eOperXslCacheInitStarted);

	static TXslEnum  enum_ = CXslDataCache::GetPaths();
	static size_t counter_ = -1;

	counter_ = 0;

	std::for_each(
		::std::begin(enum_), ::std::end(enum_), [this](TXslFile _file)
	{
		Uri^ uri_ = ref new Uri(
			ref new Platform::String(_file.second.c_str())
		);

		create_task(StorageFile::GetFileFromApplicationUriAsync(uri_))
			.then([this](task<StorageFile^> file_obj)
		{
			return create_task(Windows::Storage::FileIO::ReadTextAsync(file_obj.get()));
		},
				task_continuation_context::use_current())
			.then([this, _file](task<Platform::String^> _raw_xsl)
		{
			try
			{
				::std::wstring content_ = _raw_xsl.get()->Data();

				CXslDataCache::m_xsl_cache.insert(
					::std::make_pair(_file.first, content_)
				);
				
				if (++counter_ ==enum_.size())
					m_sink.IGenericProviderSink_OnFinish(eOperation::eOperXslCacheInitFinished);
			}
			catch (::std::bad_alloc&)
			{
				m_error.SetStatus(
					E_OUTOFMEMORY,
					_T("Cannot initialize XSL style sheet cache")
				);
				m_sink.IGenericProviderSink_OnError(m_error);
			}
		});
	});
}

bool             CXslDataCache::IsInitialized(void)const
{
	return (CXslDataCache::m_xsl_cache.empty() == false);
}

/////////////////////////////////////////////////////////////////////////////

const TXslEnum   CXslDataCache::GetPaths(void)
{
	TXslEnum enum_;
	if (enum_.empty())
	{
		LPCTSTR lpszXslName[] = {
			_T("_data_sorted_by_cat_act.xsl"),
			_T("_data_sorted_by_cat_all.xsl"),
			_T("_data_sorted_by_dat_act.xsl"),
			_T("_data_sorted_by_dat_all.xsl"),
		};

		for (INT i_ = 0; i_ < _countof(lpszXslName); i_++)
		{
			std::wstringstream path_;
			path_ << _T("ms-appx:///Assets/xslt/") << lpszXslName[i_];

			try
			{
				enum_.insert(
					::std::make_pair(lpszXslName[i_], path_.str())
				);
			}
			catch (::std::bad_alloc&)
			{
				break;
			}
		}
	}
	return enum_;
}