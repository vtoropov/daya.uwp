/*
	Created by Tech_dog (VToropov) on 30-Oct-2016 at 11:01:49p, UTC+7, Phuket, Rawai, Sunday;
	This is Daya Dimensi Global UWP application XML data provider class(es) implementation file.
*/
#include "pch.h"
#include "XmlDataProvider.h"

using namespace Daya_ToDo;
using namespace Daya_ToDo::Data;
using namespace Daya_ToDo::Data::Xml;

using namespace Windows::Data::Xml::Dom;
using namespace Windows::Data::Xml::Xsl;

#include <ppltasks.h> 
using namespace concurrency;

using namespace Windows::Foundation;
using namespace Windows::Storage;
using namespace Platform::Collections;

/////////////////////////////////////////////////////////////////////////////

namespace _impl {
	static std::wstring   result_;  // the last result of data transformation;
	static INT       overdue_c= 0;  // the last overdue days;
	static INT       overdue_p= 0;  // the previous value of overdue days for indicating changes;
}

/////////////////////////////////////////////////////////////////////////////

CXmlDataQuery::CXmlDataQuery(void)
{
}

/////////////////////////////////////////////////////////////////////////////

TXmlQryParams   CXmlDataQuery::Prepare(const CUserProfile& _profile)
{
	TXmlQryParams params_;

	LPCTSTR lpszParamNames[] = {
		_T("${global_cid_value}"),
		_T("${global_usr_value}"),
		_T("${global_pwd_value}"),
	};

	try
	{
		params_.insert(
			::std::make_pair(std::wstring(lpszParamNames[0]), _profile.CoIdentifier())
		);
		params_.insert(
			::std::make_pair(std::wstring(lpszParamNames[1]), _profile.UserName())
		);
		params_.insert(
			::std::make_pair(std::wstring(lpszParamNames[2]), _profile.Password())
		);
	}
	catch (::std::bad_alloc&)
	{
	}

	return params_;
}

/////////////////////////////////////////////////////////////////////////////

CXmlDataExtension::CXmlDataExtension(void)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef         CXmlDataExtension::Error(void)const
{
	return m_error;
}

HRESULT           CXmlDataExtension::ExtractData(const std::wstring& _raw_xml_data)
{
	m_error = S_OK;
	_impl::overdue_p = _impl::overdue_c;
	_impl::overdue_c = 0;

	XmlDocument^ doc_ = ref new XmlDocument();

	try
	{
		doc_->LoadXml(ref new Platform::String(_raw_xml_data.c_str()));
		
		XmlNodeList^ todos_ = doc_->GetElementsByTagName("d:todos");
		for (UINT j_ = 0; j_ < todos_->Length; j_++)
		{
			XmlNodeList^ elements_ = todos_->Item(j_)->ChildNodes;
			for (UINT i_ = 0; i_ < elements_->Length; i_++)
			{
				IXmlNode^ element_ = elements_->Item(i_);
				XmlNodeList^ children_ = element_->ChildNodes;
				for (UINT l_ = 0; l_ < children_->Length; l_++)
				{
					IXmlNode^ child_ = children_->Item(l_);
					Platform::String^ name_ = child_->NodeName;
					if (name_ != "d:dueDateOffSet")
						continue;
					IXmlNode^ value_ = child_->FirstChild;

					Platform::Object^ object_ = value_->NodeValue;
					Platform::String^ text_ = object_->ToString();
					const INT n_offset = _wtoi(text_->Data());
					if (n_offset > 0)
						_impl::overdue_c += 1;
					break; // only one offset per element node;
				}
			}
		}
		/*
		Platform::String^ namespaces_ = ref new Platform::String();
		namespaces_ += "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";
		namespaces_ += " ";
		namespaces_ += "http://schemas.microsoft.com/ado/2007/08/dataservices";

		XmlNodeList^ offsets_ = doc_->SelectNodesNS("/feed/entry/content/m:properties/d:todos/d:element/d:dueDateOffSet", namespaces_);
		const UINT count_ = offsets_->Length;
		for ( UINT j_ = 0; j_ < count_; j_++)
		{}
		*/
	}
	catch (Platform::COMException^ _ex) {
		m_error = _ex;
	}
	catch (Platform::Exception^ _ex) {
		m_error = _ex;
	}

	return  m_error;
}

INT               CXmlDataExtension::OverdueDays(void)const
{
	return _impl::overdue_c;
}

bool              CXmlDataExtension::OverdueDaysChanged(void)const
{
	return (_impl::overdue_c != _impl::overdue_p && _impl::overdue_c > 0);
}

VOID              CXmlDataExtension::OverdueDaysChanged(const bool _changed)
{
	if (_changed)
		_impl::overdue_p = 0;
	else
		_impl::overdue_p = _impl::overdue_c;
}

/////////////////////////////////////////////////////////////////////////////

namespace Daya_ToDo {
	namespace Data {
		namespace details {

			VOID  CXmlDataProvider_ApplyQueryParam(const TXmlQryParams& _params, std::wstring& _raw_xsl)
			{
				for (TXmlQryParams::const_iterator it_ = _params.begin(); it_ != _params.end(); ++it_)
				{
					std::wstring::size_type pos_ = _raw_xsl.find(it_->first);
					if (std::wstring::npos == pos_)
						continue;
					_raw_xsl.replace(
						pos_, ::wcslen(it_->first.c_str()), it_->second
					);
				}
			}

			VOID  CXmlDataProvider_FilterRawData(std::wstring& _raw_xml)
			{
				static LPCTSTR lpszPlaceholder[] =
				{
					_T("xmlns:m=\"http://schemas.microsoft.com/ado/2007/08/dataservices/metadata\""),
					_T("xmlns:d=\"http://schemas.microsoft.com/ado/2007/08/dataservices\"")         ,
					_T("xmlns=\"http://www.w3.org/2005/Atom\"")                                     ,
					_T("xml:base=\"https://api10.successfactors.com/odata/v2/\"")                   ,
				};
				static LPCTSTR lpszReplacement[] = {
					_T("xmlns:m=\"urn:1\"") ,
					_T("xmlns:d=\"urn:2\"") ,
					_T("")                  ,
					_T("")                  ,
				};

				for (INT i_ = 0; i_ < _countof(lpszPlaceholder) && i_ < _countof(lpszReplacement); i_++)
				{
					const std::wstring::size_type pos_ = _raw_xml.find(lpszPlaceholder[i_]);
					if (std::wstring::npos == pos_)
						continue;
					_raw_xml.replace(pos_, ::wcslen(lpszPlaceholder[i_]), lpszReplacement[i_]);
				}
			}
} } }

/////////////////////////////////////////////////////////////////////////////

CXmlDataProvider::CXmlDataProvider(IGenericProviderSink& _sink) : m_sink(_sink), m_resolver(_sink), m_cache(_sink)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef       CXmlDataProvider::Error(void)const
{
	return m_error;
}

HRESULT         CXmlDataProvider::Transform(const std::wstring& _raw_xml_data, const std::wstring& _raw_xsl_data, const TXmlQryParams& _params)
{
	m_error = S_OK;
	XmlDocument^ xml_ = ref new XmlDocument();
	XmlDocument^ xsl_ = ref new XmlDocument();
	
	std::wstring xml_data_ = _raw_xml_data;
	std::wstring xsl_data_ = _raw_xsl_data;

	details::CXmlDataProvider_FilterRawData(xml_data_);
	details::CXmlDataProvider_ApplyQueryParam(_params, xsl_data_);

	m_resolver.ResolveImport(xsl_data_);

	try
	{
		xml_->LoadXml(ref new Platform::String(xml_data_.c_str()));
		xsl_->LoadXml(ref new Platform::String(xsl_data_.c_str()));

		XsltProcessor^ proc_ = ref new XsltProcessor(xsl_);
		_impl::result_ = proc_->TransformToString(xml_)->Data();
	}
	catch (Platform::COMException^ _ex)
	{
		m_error = _ex;
	}
	catch (Platform::Exception^ _ex)
	{
		m_error = _ex;
	}

	return m_error;
}

LPCTSTR         CXmlDataProvider::Result(void)const
{
	return _impl::result_.c_str();
}

/////////////////////////////////////////////////////////////////////////////

const
CXslLinkResolver& CXmlDataProvider::Resolver(void)const
{
	return m_resolver;
}

CXslLinkResolver& CXmlDataProvider::Resolver(void)
{
	return m_resolver;
}

const
CXslDataCache&    CXmlDataProvider::XslCache(void)const
{
	return m_cache;
}

CXslDataCache&    CXmlDataProvider::XslCache(void)
{
	return m_cache;
}