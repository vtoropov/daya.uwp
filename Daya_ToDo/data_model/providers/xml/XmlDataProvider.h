#ifndef _DAYAXMLDATAPROVIDER_H_10366B71_3008_4C43_8FB6_F57A8B8B189A_INCLUDED
#define _DAYAXMLDATAPROVIDER_H_10366B71_3008_4C43_8FB6_F57A8B8B189A_INCLUDED
/*
	Created by Tech_dog (VToropov) on 30-Oct-2016 at 10:43:46p, UTC+7, Phuket, Rawai, Sunday;
	This is Daya Dimensi Global UWP application XML data provider class(es) declaration file.
*/
#include "UserProfile.h"
#include "CommonModel.h"
#include "XslLinkResolver.h"
#include "XslDataCache.h"

namespace Daya_ToDo {
	namespace Data {
		namespace Xml
		{
			typedef ::std::map<std::wstring, std::wstring> TXmlQryParams;

			class CXmlDataQuery
			{
			public:
				CXmlDataQuery(void);
			public:
				static TXmlQryParams  Prepare(const CUserProfile&);
			};

			class CXmlDataExtension
			{
			private:
				CSysError          m_error;
			public:
				CXmlDataExtension(void);
			public:
				TErrorRef          Error(void)const;
				HRESULT            ExtractData(const std::wstring& _raw_xml_data);
				INT                OverdueDays(void)const;
				bool               OverdueDaysChanged(void)const;
				VOID               OverdueDaysChanged(const bool _changed);
			};

			class CXmlDataProvider
			{
			private:
				CSysError          m_error;
			private:
				IGenericProviderSink& m_sink;
				CXslLinkResolver      m_resolver;
				CXslDataCache         m_cache;
			public:
				CXmlDataProvider(IGenericProviderSink&);
			public:
				TErrorRef          Error(void)const;
				HRESULT            Transform(const std::wstring& _raw_xml_data, const std::wstring& _raw_xsl_data, const TXmlQryParams&);
				LPCTSTR            Result(void)const;
			public:
				const
				CXslLinkResolver&  Resolver(void)const;
				CXslLinkResolver&  Resolver(void);
				const
				CXslDataCache&     XslCache(void)const;
				CXslDataCache&     XslCache(void);
			};
		}
	}
}

#endif/*_DAYAXMLDATAPROVIDER_H_10366B71_3008_4C43_8FB6_F57A8B8B189A_INCLUDED*/