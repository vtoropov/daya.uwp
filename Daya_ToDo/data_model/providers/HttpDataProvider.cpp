/*
	Created by Tech_dog (VToropov) on 30-Oct-2016 at 1:41:51p, UTC+7, Phuket, Rawai, Sunday;
	This is Daya Dimensi Global UWP application HTTP data provider class implementation file.
*/
#include "pch.h"
#include "HttpDataProvider.h"

using namespace Daya_ToDo;
using namespace Daya_ToDo::Data;

using namespace Windows::Web::Http;
using namespace Windows::Web::Http::Filters;
using namespace Windows::Foundation;

#include <ppltasks.h> 
using namespace concurrency;

#include <sstream>

/////////////////////////////////////////////////////////////////////////////

namespace _impl {
	static std::wstring data_cache_; // last raw xml data
}
/////////////////////////////////////////////////////////////////////////////

CHttpDataProvider::CHttpDataProvider(IGenericProviderSink& _sink) : m_sink(_sink), m_busy(false)
{
}

CHttpDataProvider::~CHttpDataProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef      CHttpDataProvider::Error(void)const
{
	return m_error;
}

bool           CHttpDataProvider::IsBusy(void)const
{
	return m_busy;
}

HRESULT        CHttpDataProvider::RetrieveData(const CUserProfile& _profile)
{
	if (this->IsBusy())
		return HRESULT_FROM_WIN32(ERROR_BUSY);
	else
		m_busy = true;

	HttpRequestMessage^ httpRequestMessage =
		ref new HttpRequestMessage(
			HttpMethod::Get, ref new Uri("https://api10.successfactors.com/odata/v2/Todo")
		);

	Platform::String^ auth_ = _profile.AuthenticateSeq();

	httpRequestMessage->Headers->Append(
		"Authorization",
		"Basic " + auth_
	);

	m_sink.IGenericProviderSink_OnBegin(eOperation::eOperHttpQueryStarted);

	auto filter_ = ref new HttpBaseProtocolFilter();
	filter_->CacheControl->ReadBehavior = HttpCacheReadBehavior::MostRecent;
	filter_->CacheControl->WriteBehavior = HttpCacheWriteBehavior::NoCache;

	HttpClient^ httpClient = ref new HttpClient(filter_);

	IAsyncOperationWithProgress<HttpResponseMessage^, HttpProgress>^ query_ =
		httpClient->SendRequestAsync(httpRequestMessage, HttpCompletionOption::ResponseContentRead);

	auto operationTask = create_task(query_);

	operationTask.then([this, &httpClient](HttpResponseMessage^ response)
	{
		if (response->StatusCode == HttpStatusCode::Ok)
		{
			try
			{
				auto asyncOperationWithProgress = response->Content->ReadAsStringAsync();
				create_task(asyncOperationWithProgress).then([this, &httpClient](Platform::String^ _raw_data)
				{
					_impl::data_cache_ = _raw_data->Data();
					m_sink.IGenericProviderSink_OnFinish(eOperation::eOperHttpQueryFinished); // TODO: make it safe;
					m_busy = false;
				});
			}
			catch (Platform::Exception^ _ex)
			{
				m_error = _ex;
				m_sink.IGenericProviderSink_OnError(m_error);
				m_busy = false;
			}
		}
		else
		{
			::std::wstringstream str_;
			str_ << "Web service returned error code: " << (INT)response->StatusCode;

			m_error.SetStatus(
				HRESULT_FROM_WIN32(ERROR_REQUEST_ABORTED),
				str_.str().c_str()
			);
			m_sink.IGenericProviderSink_OnError(m_error);
		}
	});

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

const std::wstring& CHttpDataProvider::Cache(void)
{
	return _impl::data_cache_;
}