/*
	Created by Tech_dog (VToropov) on 30-Oct-2016 at 2:24:19p, UTC+7, Phuket, Rawai, Sunday;
	This is Daya Dimensi Global UWP application common data model class(es) implementation file.
*/
#include "pch.h"
#include "CommonModel.h"

using namespace Daya_ToDo;
using namespace Daya_ToDo::Data;

#include <sstream>

/////////////////////////////////////////////////////////////////////////////

namespace Daya_ToDo { namespace details
{
	::std::wstring CSysError_CharToWchar(const char* _ch)
	{
		::std::string details_a = _ch;

		INT size_needed = ::MultiByteToWideChar(
							CP_UTF8, 0, &details_a[0], (INT)details_a.size(), NULL, 0
						);
		std::wstring w_string( size_needed, 0 );

		::MultiByteToWideChar(
							CP_UTF8, 0, &details_a[0], (INT)details_a.size(), &w_string[0], size_needed
						);
		return w_string;
	}
}}

/////////////////////////////////////////////////////////////////////////////

CSysError::CSysError(void) : m_result(S_OK)
{
}

CSysError::CSysError(Platform::Exception^ _ex) : m_result(S_OK)
{
	m_result = _ex->HResult;
	m_details = _ex->Message->Data();
}

CSysError::CSysError(const ::std::exception& _ex) : m_result(S_OK)
{
	m_result = E_FAIL;
	m_details = details::CSysError_CharToWchar(_ex.what());
}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR       CSysError::GetDetails(void) const
{
	return m_details.c_str();
}

std::wstring  CSysError::GetFormattedDetails(const bool bMultiline)const
{
	std::wstringstream stream_;
	stream_ << _T("code=0x") << std::hex << this->GetHresult();

	if (bMultiline) stream_  << std::endl;
	else            stream_  << _T("; ");
	
	stream_ << _T("desc=")   << this->GetDetails();

	std::wstring formatted_ = stream_.str();
	return formatted_;
}

HRESULT       CSysError::GetHresult(void) const
{
	return m_result;
}

VOID          CSysError::SetStatus(const HRESULT _hr, LPCTSTR lpszDetails)
{
	m_result = _hr;
	m_details = lpszDetails;
}
/////////////////////////////////////////////////////////////////////////////

CSysError&    CSysError::operator= (const HRESULT _hr)
{
	m_result = _hr;
	m_details = _hr.ToString()->Data();
	return *this;
}

CSysError&    CSysError::operator= (const ::std::exception& _ex)
{
	m_result = E_FAIL;
	m_details = details::CSysError_CharToWchar(_ex.what());
	return *this;
}

CSysError&    CSysError::operator=(Platform::Exception^ _ex)
{
	m_result = _ex->HResult;
	m_details = _ex->Message->Data();
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CSysError::operator bool(void)const
{
	return (FAILED(m_result));
}

CSysError::operator HRESULT(void) const
{
	return m_result;
}