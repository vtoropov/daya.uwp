﻿#ifndef _DAYATODOSTDAFX_H_C865AF1A_5DD4_4952_8915_C2A1D99F8555_INCLUDED
#define _DAYATODOSTDAFX_H_C865AF1A_5DD4_4952_8915_C2A1D99F8555_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Oct-2016 at 10:14:53p, GMT+7, Phuket, Rawai, Wednesday;
	This is a precompiled header for standard system include files.
*/
#include <collection.h>
#include <ppltasks.h>

#include "App.xaml.h"

#define _T(x)      L ## x

namespace task_ext
{
	// runs a function in the main thread with non-interactive priority;
	void run_async_non_interactive(std::function<void()>&& action);
}

namespace global
{
	using Windows::UI::Core::CoreDispatcher;
	CoreDispatcher^ get_core_dispatcher(void);
}

#endif/*_DAYATODOSTDAFX_H_C865AF1A_5DD4_4952_8915_C2A1D99F8555_INCLUDED*/