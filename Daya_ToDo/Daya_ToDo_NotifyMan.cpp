/*
	Created by Tech_dog (VToropov) on 6-Dec-2016 10:33:49a, UTC+7, Phuket, Rawai, Tuesday;
	This is Daya Dimensi Global UWP application notification manager class implementation file.
*/
#include "pch.h"
#include "Daya_ToDo_NotifyMan.h"

using namespace Daya_ToDo;
using namespace Daya_ToDo::Data;
using namespace Daya_ToDo::notify;

std::wstring CNotifyManager::m_cache;

#include <ppltasks.h> 
using namespace concurrency;

using namespace Windows::Foundation;
using namespace Windows::Storage;

using namespace Windows::UI::Notifications;
using namespace Windows::Data::Xml::Dom;

#include <string>
#include <sstream>

/////////////////////////////////////////////////////////////////////////////

namespace Daya_ToDo { namespace notify { namespace details
{
	VOID CToastNotify_FormatMessage(const INT _overdue, ::std::wstring& _message)
	{
		// (1) replaces number of overdue events;
		{
			LPCTSTR lpszPlaceholder = _T("{$evt_number}");
			std::wstring::size_type pos_ = _message.find(lpszPlaceholder);
			if (std::wstring::npos != pos_)
				_message.replace(
					pos_, ::wcslen(lpszPlaceholder), ::std::to_wstring(_overdue)
				);
		}
		// (2) changes suffix of 'event(s)' noun
		{
			LPCTSTR lpszPlaceholder = _T("{$evt_suffix}");
			std::wstring::size_type pos_ = _message.find(lpszPlaceholder);
			if (std::wstring::npos != pos_)
			{
				if (_overdue > 1)
				_message.replace(
					pos_, ::wcslen(lpszPlaceholder), _T("s")
				);
				else
				_message.replace(
					pos_, ::wcslen(lpszPlaceholder), _T("")
				);
			}
		}
		// (3) changes suffix of 'require' verb
		{
			LPCTSTR lpszPlaceholder = _T("{$req_suffix}");
			std::wstring::size_type pos_ = _message.find(lpszPlaceholder);
			if (std::wstring::npos != pos_)
			{
				if (_overdue < 2)
					_message.replace(
						pos_, ::wcslen(lpszPlaceholder), _T("s")
					);
				else
					_message.replace(
						pos_, ::wcslen(lpszPlaceholder), _T("")
					);
			}
		}
		// (4) sets appropriate image reference
		{
			LPCTSTR lpszPlaceholder = _T("{$image_file}");
			std::wstring::size_type pos_ = _message.find(lpszPlaceholder);
			if (std::wstring::npos != pos_)
			{
				std::wstringstream stream_;
				stream_ << _T("ddg_notify_");
				if      (_overdue < 10) { stream_ << _T("0")  <<  _overdue; }
				else if (_overdue > 13) { stream_ << _T("##"); }
				else                    { stream_ << _overdue; }
				stream_ << _T("_evt.png");

				_message.replace(
					pos_, ::wcslen(lpszPlaceholder), stream_.str()
				);
			}
		}
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CNotifyManager::CNotifyManager(IGenericProviderSink& _sink) : m_evt_sink(_sink)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CNotifyManager::Error(void)const
{
	return m_error;
}

HRESULT      CNotifyManager::InitializeAsync(void)
{
	m_error = S_OK;

	if (CNotifyManager::m_cache.empty() == false)
		return m_error;

	m_evt_sink.IGenericProviderSink_OnBegin(eOperation::eOperNotifyInitStarted);

	Uri^ uri_ = ref new Uri(
		ref new Platform::String(_T("ms-appx:///Assets/xml/Daya_ToastNotification.xml"))
	);
	create_task(StorageFile::GetFileFromApplicationUriAsync(uri_))
	.then([this](task<StorageFile^> file_obj)
	{
		return create_task(Windows::Storage::FileIO::ReadTextAsync(file_obj.get()));
	},
	task_continuation_context::use_current())
	.then([this](task<Platform::String^> _raw_xml)
	{
		try
		{
			CNotifyManager::m_cache = _raw_xml.get()->Data();
			m_evt_sink.IGenericProviderSink_OnFinish(eOperation::eOperNotifyInitFinished);
		}
		catch(std::exception& _ex)
		{
			m_error = _ex;
			m_evt_sink.IGenericProviderSink_OnError(m_error);
		}
	});
	return m_error;
}

HRESULT      CNotifyManager::TileNotify (const INT _overdue)
{
	m_error = S_OK;

	static INT overdue_p = -1;
	if (_overdue == overdue_p)
		return m_error;

	overdue_p = _overdue;

	if (_overdue < 1)
	{
		BadgeUpdateManager::CreateBadgeUpdaterForApplication()->Clear();
	}
	else
	{
		try
		{
			auto xml_ = "<badge value='" + _overdue + "'/>";

			XmlDocument^ doc_ = ref new XmlDocument();
			doc_->LoadXml(xml_);

			BadgeNotification^ notify_ = ref new BadgeNotification(doc_);
			BadgeUpdateManager::CreateBadgeUpdaterForApplication()->Update(notify_);
		}
		catch(Platform::Exception^ _ex)
		{
		}
	}
	return m_error;
}

HRESULT      CNotifyManager::ToastNotify(const INT _overdue)
{
	m_error = S_OK;
	if (CNotifyManager::m_cache.empty() == true)
		return (m_error = OLE_E_BLANK);

	std::wstring cs_toast = CNotifyManager::m_cache; // makes a template copy;
	details::CToastNotify_FormatMessage(_overdue, cs_toast);

	XmlDocument^ toastXml = ref new XmlDocument();
	toastXml->LoadXml(ref new Platform::String(cs_toast.c_str()));

	auto notification_ = ref new ToastNotification(toastXml);
	notification_->Tag = "1ff9d9e5";
	notification_->Group = "generic_notify";
	
	ToastNotifier^ notifier_ = ToastNotificationManager::CreateToastNotifier();
	notifier_->Show(notification_);

	return m_error;
}