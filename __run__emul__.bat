::
:: Created by Tech_dog (VToropov) on 28-Oct-2016 at 2:31:33p, GMT+7, Phuket, Rawai, Friday;
:: This is Daya Dimensi Global UWP app test Windows Phone emulator launching batch file.
::

SET EMUL_NAME="Emulator 10.0.1.0 WVGA 4 inch 512MB.rob"
SET DISP_NAME="Emulator 10.0.1.0 WVGA 4 inch 512MB"

SET EMUL_EXE="C:\Program Files (x86)\Microsoft XDE\10.0.14393.0\XDE.exe"
SET FLASH_DISK="C:\Program Files (x86)\Windows Kits\10\Emulation\Mobile\10.0.14393.0\flash.vhd"
SET VIRTUAL_DISK="C:\Users\ebont\AppData\Local\Microsoft\XDE\10.0.14393.0\dd.480x854.512.vhd"

%EMUL_EXE% /name %EMUL_NAME% /displayName %DISP_NAME% /vhd %FLASH_DISK% /video "480x854" /memsize 512 /diagonalSize 4 /language 409 /creatediffdisk %VIRTUAL_DISK% /snapshot /fastShutdown
